#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import threading
import time

from gi.repository import GLib

from dsociepy.finance.framework.market_interfaces.exceptions import APINotAvailable
from dsociepy.finance.framework.market_interfaces.exceptions import APIErrors
from dsociepy.finance.framework.market_interfaces.exceptions import EndOfSimulation
from dsociepy.finance.framework.market_interfaces.exceptions import ResponseIsNotJSON

class Conductor(threading.Thread):
    def __init__(self, market, timer=time.time(), index=0):
        threading.Thread.__init__(self)
        self.__market = market
        self.__configuration = market.states["markets"][market.name]["setup"]
        self.daemon = True
        self.__timer = timer
        self.__index = index

    def get_states(self):
        return (self.__timer, self.__index)

    def decrement_index(self):
        self.__index -= 1

    def run(self):
        if len(self.__market.trades) != 0 and self.__configuration != {}:
            self.__real()
            self.__backtest()

    """ Market should returns trades with locks """
    def __backtest(self):
        # THIS IS A SIMULATION WE DON'T CARE ABOUT delaying
        """ might race """
        trades = [trade for trade in self.__market.trades if trade.is_backtest()]
        for trade in trades:
            if trade.running:
                try:
                    trade.perform_strategy()
                            
                except EndOfSimulation as e:
                    trade.update_log(e)
                    trade.running = False
                    GLib.idle_add(trade.user_interface.control.set_label, "Run")
                    
    def __real(self):
        """ might race """
        trades = [trade for trade in self.__market.trades if not trade.is_backtest()]
        if len(trades) !=0:
            if self.__timer + self.__configuration["Period"] < time.time():
                if self.__index >= len(trades):
                    self.__index = 0

                trade = trades[self.__index]
                if trade.running:
                    try:
                        trade.perform_strategy()

                    except Exception as e:
                        trade.update_log(e)

                    self.__timer = time.time()

                self.__index += 1

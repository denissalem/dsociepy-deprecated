#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import os
import datetime

from dsociepy.finance.framework.helpers import MARKET_DATA_FOLDER
from dsociepy.finance.framework.helpers import TIMEFRAMES
from dsociepy.finance.framework.helpers import BASE_TIMEFRAMES_PER_MARKET
from dsociepy.finance.framework.helpers import set_ohlc_per_filenames
from dsociepy.finance.framework.helpers import synthetize_ohlc
from dsociepy.finance.framework.helpers import write_ohlc_per_filenames
from dsociepy.finance.framework.helpers import lookup_timeframe_dependancy

def fill_empty_spaces(data_set, timeframe):
    new_data_set = []
    if len(data_set):
        previous = data_set[0]
        new_data_set.append(previous)
        for ohlc in data_set[1:]:
            if ohlc[0] == previous[0] + timeframe:
                new_data_set.append(ohlc)
            # fill empty space
            else:
                diff = ohlc[0] - previous[0]
                if diff <= 0:
                    pass

                else:
                    missing_count = (diff // timeframe) - 1
                    for i in range(1, missing_count+1):
                        new_data_set.append([
                            previous[0] + i * timeframe
                        ])
                    new_data_set.append(ohlc)
            previous = ohlc

    return new_data_set

def check_data_contiguity(data, timeframe, previous = None):
    if previous == None:
        previous = data[0]
        offset=1
    
    else:
        offset=0

    previous_timestamp = previous[0]
    missing_count = 0
    missing_index = -1
    i = 0
    for d in data[offset:]:
        if d[0] != previous_timestamp + timeframe:
            if d[0] < previous_timestamp:
                missing_count += 1

            else:
                missing_count += ((d[0] - previous_timestamp) / timeframe)-1

        elif len(d) == 1:
            missing_count += 1
            
        if d[0] != previous_timestamp + timeframe or len(d) == 1:
            if missing_index == -1:
                missing_index = offset + i

        i+=1                    
        previous = d
        previous_timestamp = d[0]
    
    return (previous, missing_count, missing_index)

def check_datastore_timeframes():
    must_repair = []
    for market in os.listdir(MARKET_DATA_FOLDER):
        for pair in os.listdir(MARKET_DATA_FOLDER+'/'+market):
            base_timeframe = BASE_TIMEFRAMES_PER_MARKET[market]
            timeframe_folders = os.listdir(MARKET_DATA_FOLDER+'/'+market+'/'+pair)
            for timeframe in sorted(TIMEFRAMES.keys()):
                if timeframe > base_timeframe:
                    if not str(timeframe) in timeframe_folders:
                        must_repair.append((market,pair,timeframe))
                    
    return must_repair
            
def restore_timeframe(market, pair, timeframe):
    lower_timeframe = lookup_timeframe_dependancy(timeframe)
    os.makedirs(MARKET_DATA_FOLDER+'/'+market+'/'+pair+'/'+str(lower_timeframe), exist_ok=True)
    
    for filename in os.listdir(MARKET_DATA_FOLDER+'/'+market+'/'+pair+'/'+str(lower_timeframe)):
        timestamp = datetime.datetime.strptime(
            filename.replace(".json",''),
            TIMEFRAMES[lower_timeframe]
        ).timestamp()

        data_source = synthetize_ohlc(MARKET_DATA_FOLDER+'/'+market, pair, timeframe, timestamp)
        filenames = set_ohlc_per_filenames(data_source, timeframe)
        write_ohlc_per_filenames(MARKET_DATA_FOLDER+'/'+market+'/'+pair+'/'+str(timeframe)+'/', filenames)



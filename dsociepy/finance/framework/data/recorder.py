#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import datetime
import json
import os
import threading
import time

from dsociepy.finance.framework.data.integrity import check_data_contiguity
from dsociepy.finance.framework.data.integrity import fill_empty_spaces
from dsociepy.finance.framework.data.ohlc import OHLC
from dsociepy.finance.framework.helpers import date_from_time_stamp
from dsociepy.finance.framework.helpers import get_market_data_filename
from dsociepy.finance.framework.helpers import MARKET_DATA_FOLDER
from dsociepy.finance.framework.helpers import TIMEFRAMES
from dsociepy.finance.framework.helpers import get_timestamp_by_timeframe
from dsociepy.finance.framework.helpers import get_timestamp_by_filename
from dsociepy.finance.framework.helpers import set_ohlc_per_filenames
from dsociepy.finance.framework.helpers import synthetize_ohlc
from dsociepy.finance.framework.helpers import write_ohlc_per_filenames

class Recorder(threading.Thread):
    def __init__(self, asset_pairs, market_name, remote_market_interface, log_callback, locks):
        threading.Thread.__init__(self)
        self.__locks = locks
        self.__log_callback = log_callback
        self.__market_name = market_name
        self.__asset_pairs = asset_pairs # Required for acquire remote data
        self.__remote_market_interface = remote_market_interface[0]() # Required for acquire remote data
        self.__remote_market_interface_delay = remote_market_interface[1] # Required for acquire remote data
        self.__market_data_folder = MARKET_DATA_FOLDER+'/'+market_name # Required for saving data
        
        self.__since = {}
        self.__raw_market_data = [] # Remote data are dumped first there.
        self.__running = False

    def __log(self, e):
        self.__log_callback(self.__market_name, e)

    def __setup_persistent_timeframe(self, asset_pair, timeframe):
        filenames = dict()
        destination_folder = self.__market_data_folder+'/'+asset_pair+'/'+str(timeframe)+'/'
        os.makedirs(destination_folder, exist_ok=True)
        
        # Compute timeframe
        timeframes = sorted(list(TIMEFRAMES.keys()))
        base_timeframe_index = timeframes.index(self.__remote_market_interface.base_timeframe)
        timeframe_index = timeframes.index(timeframe)
        if timeframe_index > base_timeframe_index:
            timestamps = []
            data_source = []
            for ohlc in self.__raw_market_data:
                new = get_timestamp_by_filename(
                    get_market_data_filename(ohlc[0], timeframes[timeframe_index-1]).replace(".json",''),
                    timeframes[timeframe_index-1]
                )
                if not new in timestamps:
                    timestamps.append(new)

            for t in timestamps:
                data_source += synthetize_ohlc(self.__market_data_folder, asset_pair, timeframe, t)
            
        else:
            # we're dealing with base_timeframe
            data_source = self.__raw_market_data

        # Build a dict : filename => ohlc data
        filenames = set_ohlc_per_filenames(data_source, timeframe)
        write_ohlc_per_filenames(destination_folder, filenames)

    def __acquire_remote_market_data(self, asset_pair):
        try:
            since = self.__since[asset_pair]

        except KeyError:
            self.__since[asset_pair] = 0
            since = self.__since[asset_pair]

        """ Acquire remote market data, must be thread safe. """
        raw_market_data = self.__remote_market_interface.get_ohlcv(
            asset_pair,
            since - (4 * self.__remote_market_interface.base_timeframe)
        )
        self.__raw_market_data = sorted(raw_market_data, key=lambda d : d[0])
        
        if len(self.__raw_market_data) > 0:
            p, mc, mi = check_data_contiguity(self.__raw_market_data, 60)
            print(asset_pair, len(self.__raw_market_data), mc)
            
            # SETUP SINCE VARIABLE
            self.__since[asset_pair] = self.__raw_market_data[mi][0]

    def run(self):
        self.__running = True
        while self.__running:
            asset_pair_index = 0
            while asset_pair_index < len(self.__asset_pairs):
                if not self.__running:
                    return

                try:
                    self.__acquire_remote_market_data(self.__asset_pairs[asset_pair_index])
                    if len(self.__raw_market_data) > 0:
                        base_timeframe = self.__remote_market_interface.base_timeframe
                        for timeframe in TIMEFRAMES:
                            if timeframe >= base_timeframe:
                                try:
                                    self.__locks[self.__market_name][self.__asset_pairs[asset_pair_index]][str(timeframe)].acquire()
                                    self.__setup_persistent_timeframe(self.__asset_pairs[asset_pair_index], timeframe)
                                    self.__locks[self.__market_name][self.__asset_pairs[asset_pair_index]][str(timeframe)].release()
                                
                                except Exception as e:
                                    self.__locks[self.__market_name][self.__asset_pairs[asset_pair_index]][str(timeframe)].release()
                                    raise e

                    asset_pair_index+=1
                
                except Exception as e:
                    self.__log(e)

                time.sleep(5)
            print()
            
    def terminate(self):
        self.__running = False



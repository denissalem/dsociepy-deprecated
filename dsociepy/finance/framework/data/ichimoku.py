#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from dsociepy.finance.framework.data import DataSet

class Ichimoku(DataSet):
    def __init__(self, timeframe, ohlc):
        super(Ichimoku, self).__init__(timeframe)
        self.__ohlc = ohlc
        self.__tenkan_period = 9
        self.__kijun_period = 26
        self.__projection_period = 26
        self.__ssb_period = 52

    def __compute_MA(self, timestamp, period):
        if timestamp == -1:
            return -1

        index = self.__ohlc.get_index_by_timestamp(timestamp)
        begin = index - period if index - period >= 0 else 0
        highest_high = 0
        lowest_low = 4294967296

        samples = [frame for frame in self.__ohlc.get_data()[begin+1:index+1] if len(frame) > 1]
        if len(samples) == 0:
            return -1

        for frame in samples:
            if frame[2] > highest_high:
                highest_high = frame[2]

            if frame[3] < lowest_low:
                lowest_low = frame[3]

        return (highest_high+lowest_low)/2
    
    def get_projection_periods(self):
        return (self.__ssb_period, self.__projection_period)

    def __compute_chikou_span(self, timestamp):
        if timestamp == -1:
            return -1

        try:
            index = self.__ohlc.get_index_by_timestamp(timestamp + self._timeframe * self.__projection_period)
            return self.__ohlc.get_data()[index][4]
        
        except Exception as e:
            return -1

    def __merge(self, current, ahead):
        merged = [current[0]] + [-1] * 5
        for i in range(1,len(current)):
            if current[i] != -1:
                merged[i] = current[i]

            elif ahead[i] != -1:
                merged[i] = ahead[i]

        return merged

    def update(self, frames):
        new_frames = []
        self._data = []

        """ Not really optimized for now, but it's the simpliest way to get contiguous data stream """
        frames = self.__ohlc.get_data()
        for i in range(0, len(frames)):
            frame_current = []
            frame_current.append(frames[i][0])

            frame_ahead = []
            frame_ahead.append(frames[i][0] + self._timeframe * self.__projection_period)
            frame_ahead.append(-1)
            frame_ahead.append(-1)
            frame_ahead.append(-1)
            
            tenkan = self.__compute_MA(frames[i][0], self.__tenkan_period)
            kijun = self.__compute_MA(frames[i][0], self.__kijun_period)
            if tenkan != -1 and kijun != -1:
                frame_ahead.append((tenkan + kijun)/2)
            
            else:
                frame_ahead.append(-1)

            frame_ahead.append(self.__compute_MA(frames[i][0], self.__ssb_period))

            frame_current.append(tenkan)
            frame_current.append(kijun)
            frame_current.append(self.__compute_chikou_span(frames[i][0]))
            frame_current.append(-1)
            frame_current.append(-1)
            
            try:
                index = [d[0] for d in new_frames].index(frame_ahead[0])
                merged_frame = self.__merge(new_frames[index], frame_ahead)
                new_frames[index] = merged_frame

            except Exception as e:
                new_frames.append(frame_ahead)

            try:
                index = [d[0] for d in new_frames].index(frame_current[0])
                merged_frame = self.__merge(frame_current, new_frames[index])
                new_frames[index] = merged_frame

            except Exception as e:
                new_frames.append(frame_current)
            
        super(Ichimoku, self).update(new_frames)



#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from dsociepy.finance.framework.data import DataSet

class Trades(DataSet):
    def __init__(self):
        super(Trades, self).__init__(0)
    
    def get_trades_by_range(self, timestamp_begin, timestamp_end):
        return [ trade for trade in self._data if trade[0] >= timestamp_begin and trade[0] <= timestamp_end ]
    
    def yield_trades(self, t1, t2):
        for trade in self.get_trades_by_range(t1, t2):
            yield trade



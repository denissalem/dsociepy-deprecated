#! /usr/bin/python3

#    Copyright 2016, 2017 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import datetime
import time

from dsociepy.finance.framework.data import DataSet
from dsociepy.finance.framework.helpers import get_market_data_filename

class OHLC(DataSet):
    def __init__(self, timeframe):
        super(OHLC, self).__init__(timeframe)

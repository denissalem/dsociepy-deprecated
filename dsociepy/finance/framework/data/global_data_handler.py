#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import time

from dsociepy.finance.framework.data.backtest_data_handler import BacktestDataHandler
from dsociepy.finance.framework.data.data_handler import DataHandler
from dsociepy.finance.framework.data.trades import Trades
from dsociepy.finance.framework.helpers import box_visible
from threading import Lock

class GlobalDataHandler:
    def __init__(self, get_mds_context, backtest=False):
        self.__backtest = backtest
        self.__get_mds_context = get_mds_context
        self.timestamp_horizon = 0
        self.lowest_timeframe = -1
        self.__sync_delay = 1
        self.__sync_timer = 0
        self.__lock = Lock()
        self.__data_handlers = []
        self.__trades = Trades()
        self.__range_boxes = {}

    def get_trades_handler(self):
        return self.__trades

    def push_range_begin(self, timeframe, t):
        if not timeframe in self.__range_boxes.keys():
            self.__range_boxes[timeframe] = [[t]]
            return
        
        self.__range_boxes[timeframe].append([t])
        print(self.__range_boxes)

    def push_range_end(self, timeframe, t):
        self.__range_boxes[timeframe][-1].append(t)
        print(self.__range_boxes)

    def get_range_boxes(self, timeframe, begin, end):
        try:
            boxes = self.__range_boxes[timeframe]

        except KeyError:
            return []

        return [box for box in boxes if box_visible(box, begin, end)]


    def push_trade(self, timestamp, trade, price, volume):
        self.__trades.update(
            [[timestamp, trade, price, volume, False]]
        )

    def add(self, timeframe, strategy=False):
        if self.__backtest:
            data_handler = BacktestDataHandler(self.__get_mds_context, timeframe, strategy, self)
            self.__lock.acquire()
            if strategy and (self.lowest_timeframe == -1 or self.lowest_timeframe > timeframe):
                self.lowest_timeframe = timeframe
                self.timestamp_horizon = data_handler.get_horizon()
                self.sync(strategy, lock=False)

            self.__lock.release()
        else:
            data_handler = DataHandler(self.__get_mds_context, timeframe, self)

        self.__data_handlers.append([strategy, data_handler])
        return data_handler

    def remove(self, data_handler):
        i = [d[1] for d in self.__data_handlers].index(data_handler)
        self.__data_handlers.remove(self.__data_handlers[i])

    def sync(self, strategy, lock=True):
        if lock:
            self.__lock.acquire()
        
        if self.__sync_timer + self.__sync_delay < time.time() and strategy:
            self.__sync_timer = time.time()
            if self.lowest_timeframe != -1:
                self.timestamp_horizon += self.lowest_timeframe
                
        timestamp_horizon = self.timestamp_horizon
        if lock:
            self.__lock.release()

        return timestamp_horizon



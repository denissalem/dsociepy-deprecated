#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import io
import json
import pycurl
import time
import urllib.parse

from dsociepy.finance.framework.data.ohlc import OHLC
from dsociepy.finance.framework.helpers import repair_data_set
from dsociepy.finance.framework.helpers import date_from_time_stamp #for debug
from dsociepy.finance.framework.data.ichimoku import Ichimoku
from dsociepy.finance.framework.data.volume import Volume
from dsociepy.finance.framework.data import DataSet
from dsociepy.finance.framework.data.integrity import check_data_contiguity
from dsociepy.finance.framework.market_interfaces.exceptions import ResponseIsNotJSON
from dsociepy.finance.framework.market_interfaces.exceptions import APINotAvailable
from dsociepy.finance.framework.server.exceptions import ServerError
from threading import Lock

WINDOW_SIZE_FACTOR = 3

class DataHandler:
    def __init__(self, get_mds_context, timeframe, master):
        self._master = master
        self._refreshment_delay = 0
        self._acquisition_in_progress = False
        self._request_latest_data = True
        self._request_oldest_data = False
        self._missing_data_in_request = False
        self._latest_timestamp = 0
        self._oldest_timestamp = -1
        self._timestamp_start = -1
        self._window_length = 150
        self._get_mds_context = get_mds_context
        self._timeframe = timeframe
        self._indicators = {
            "OHLC": OHLC(timeframe),
            "Volume": Volume(timeframe)
        }
        self._forward_projection = 0
        self._backward_projection = 0
        self._lock = Lock()
        self._acquisition_lock = Lock()

    def _is_delay_done(self):
        if self._refreshment_delay + 1 < time.time():
            self._refreshment_delay = time.time()
            return True

        else:
            return False

    def _get_acquire_timestamp(self, timestamp):
        if timestamp >= self._latest_timestamp:
            return -1
        
        else:
            return int(timestamp)
        
    def _acquire_data(self, timestamp, window):
        self._set_acquisition_in_progress(True)
        timestamp = self._get_acquire_timestamp(timestamp)

        if self._get_mds_context().server_name == '':
            raise APINotAvailable

        try:
            params = urllib.parse.urlencode({
                "market" : self._get_mds_context().market,
                "pair" : self._get_mds_context().pair,
                "timeframe" : self._timeframe,
                "timestamp" : timestamp,
                "window" : window * 3 + self._backward_projection * 2
            })
            #print("debug", params)
            buffer = io.BytesIO()
            query = pycurl.Curl()
            query.setopt(query.URL, self._get_mds_context().server_name+'/OHLCV?'+params)
            query.setopt(query.PORT, self._get_mds_context().port)
            query.setopt(query.WRITEFUNCTION, buffer.write)
            query.perform()
            response = buffer.getvalue().decode('UTF-8')
            buffer.close()

        except Exception as e:
            self._set_acquisition_in_progress(False)
            raise APINotAvailable(e)
        
        self._set_acquisition_in_progress(False)

        try:
            data =  json.loads(response)
            if len(data["errors"]):
                raise ServerError(data_set["errors"])

            return data

        except Exception as e: 
            raise ResponseIsNotJSON(response) 

    def _set_timestamp_and_window_length(self, timestamp_start, window_length):
        self._timestamp_start = timestamp_start
        self._window_length = window_length
        for key in self._indicators.keys():
            self._indicators[key].set_timestamp_and_window_length(timestamp_start, window_length)

    def _clear_horizon(self):
        for key in self._indicators.keys():
            self._indicators[key].clear_horizon()
    
    def _update_dataset(self, data_set):
        frames = [frame for frame in data_set["values"] ]
        self._indicators["OHLC"].update([frame[:-1] if len(frame) > 1 else [frame[0]] for frame in frames])
        self._indicators["Volume"].update([  [frame[0], frame[-1], frame[1] < frame[4]] if len(frame) > 1 else [frame[0]] for frame in frames])
                        
        for indicator in self._indicators.keys():
            if not indicator in ["OHLC","Volume"]:
                self._indicators[indicator].update(frames)

    def _is_timestamp_ahead(self):
        return self._timestamp_start >= self._latest_timestamp

    def _finalize_timestamp(self):
        if self._timestamp_start == -1 or self._timestamp_start >= self._latest_timestamp + self._forward_projection * self._timeframe:
            self._timestamp_start = self._latest_timestamp + self._forward_projection * self._timeframe

    def prepare_dataset(self, scroll, window_length):
        self._lock.acquire()

        """ Prepare timestamp """
        if self._timestamp_start != -1:
            self._timestamp_start += scroll * self._timeframe
            if (not self._is_delay_done()) and self._is_timestamp_ahead():
                self._lock.release()
                return

        if self._timestamp_start < self._oldest_timestamp:
            self._timestamp_start = self._oldest_timestamp

        if not self.is_data_available(self._timestamp_start, window_length) or self._is_timestamp_ahead():
            try:
                data_set = self._acquire_data(int(self._timestamp_start), window_length)

                oldest_t = data_set["values"][0][0]
                latest_t = data_set["values"][-1][0]
                self._clear_horizon()

                if "REQUEST_LATEST_DATA" in data_set["notices"]:
                    self._latest_timestamp = latest_t
            
                if "REQUEST_OLDEST_DATA" in data_set["notices"] and self._oldest_timestamp == 0:
                    self._oldest_timestamp = oldest_t

                if "MISSING_DATA" in data_set["notices"]:
                    if self._timestamp_start == -1 or latest_t >= self._latest_timestamp:
                        timestamp_start_repair = latest_t

                    else:
                        timestamp_start_repair = self._timestamp_start
                    
                    data_set = repair_data_set(data_set, int(timestamp_start_repair), window_length * 3 + self._backward_projection * 2, self._timeframe)
                
            except Exception as e:
                self._lock.release()
                raise e
            
            self._update_dataset(data_set)
            D = self.get_dataset("OHLC").get_data()
        
        self._finalize_timestamp()
        self._set_timestamp_and_window_length(self._timestamp_start, window_length)
        self._lock.release()

    def acquire_lock(self):
        try:
            self._lock.acquire()

        except:
            pass

    def release_lock(self):
        try:
            self._lock.release()

        except:
            pass

    def get_dataset(self, name):
        try:
            return self._indicators[name]

        except KeyError as e:
            if name == "Trades":
                return self._master.get_trades_handler()

            elif name == "Ranges":
                return self._master.get_range_boxes

            else:
                raise KeyError(name+':', "Indicator isn't binded.")
    
    def reset(self):
        for key in self._indicators:
            self._indicators[key].reset()

    def get_forward_projection(self):
        return self._forward_projection

    def get_backward_projection(self):
        return self._backward_projection

    def has_acquired_data(self):
        self._has_acquired_data_lock.acquire()
        has = self._has_acquired_data
        self._has_acquired_data_lock.release()
        return

    def get_latest_timestamp(self):
        self._lock.acquire()
        t = self._latest_timestamp
        self._lock.release()
        return t

    def get_oldest_timestamp(self):
        self._lock.acquire()
        t = self._oldest_timestamp
        self._lock.release()
        return t

    def get_timestamp_and_window_length(self):
        self._lock.acquire()
        d = (self._timestamp_start, self._window_length)
        self._lock.release()
        return d

    def get_acquisition_in_progress(self):
        self._acquisition_lock.acquire()
        state = self._acquisition_in_progress
        self._acquisition_lock.release()
        return state

    def _set_acquisition_in_progress(self, state):
        self._acquisition_lock.acquire()
        self._acquisition_in_progress = state
        self._acquisition_lock.release()

    def get_timeframe(self):
        return self._timeframe

    def bind_ichimoku(self):
        self._lock.acquire()
        self._indicators["Ichimoku"] = Ichimoku(self._timeframe, self._indicators["OHLC"])
        self._indicators["Ichimoku"].update(self._indicators["OHLC"].get_data())
        self._indicators["Ichimoku"].set_timestamp_and_window_length(self._timestamp_start, self._window_length)
        backward_projection, forward_projection = self._indicators["Ichimoku"].get_projection_periods()
        self._forward_projection = forward_projection
        self._backward_projection = backward_projection
        self._lock.release()

    def unbind_ichimoku(self):
        self._lock.acquire()
        """ must be adaptative for another kind of indicators """
        self._forward_projection = 0
        self._backward_projection = 0
        self._indicators.pop("Ichimoku")
        self._lock.release()
       
    def is_updated(self):
        self._lock.acquire()
        updated = False
        for indicator in self._indicators.keys():
            updated = updated or self._indicators[indicator].is_updated()

        self._lock.release()
        return updated

    def is_updated_by_indicator(self, indicator_key):
        self._lock.acquire()
        updated = self._indicators[indicator_key].is_updated()
        self._lock.release()
        return updated

    def is_data_available(self, timestamp_start, window_length):
        self._set_timestamp_and_window_length(
            timestamp_start,
            window_length)

        try:
            self._indicators["OHLC"].get_positions()

        except ValueError as e:
            return False
        
        return True

    def is_request_latest_data(self):
        return self._request_latest_data
    
    def is_request_oldest_data(self):
        return self._request_oldest_data


#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY. If not, see <http://www.gnu.org/licenses/>.

import json
import datetime
from dsociepy.finance.framework.data.data_handler import DataHandler
from dsociepy.finance.framework.helpers import query_oldest_ohlcv_window
from dsociepy.finance.framework.helpers import date_from_time_stamp
from dsociepy.finance.framework.market_interfaces.exceptions import ResponseIsNotJSON
from dsociepy.finance.framework.market_interfaces.exceptions import APINotAvailable

class BacktestDataHandler(DataHandler):
    def __init__(self, get_mds_context, timeframe, strategy, master):
        super(BacktestDataHandler, self).__init__(get_mds_context, timeframe, master)
        self.__strategy = strategy
        """ may be optimized """
        dataset = self.get_oldest_ohlcv_window() # get data with timeframe = 1 minute !!!
        self._latest_timestamp = (dataset["values"][-1][0] // self._timeframe) * self._timeframe
        self._timestamp_start = (dataset["values"][-1][0] // self._timeframe) * self._timeframe
        self._oldest_timestamp = (dataset["values"][0][0] // self._timeframe) * self._timeframe

        #self._update_dataset(dataset)
    
    def _get_acquire_timestamp(self, timestamp):
        if timestamp >= self._latest_timestamp:
            return int(self._latest_timestamp)
        
        else:
            return int(timestamp)
        

    def get_horizon(self):
        return self._latest_timestamp
    
    """ may be optimized """
    def get_oldest_ohlcv_window(self):
        self._set_acquisition_in_progress(True)
        try:
            response = query_oldest_ohlcv_window(self._get_mds_context, self._window_length)

        except Exception as e: 
            self._set_acquisition_in_progress(False)
            raise APINotAvailable(e)
        
        except TypeError as e:
            raise ResponseIsNotJSON(e) 
        
        self._set_acquisition_in_progress(False)
        if len(response["errors"]):
            raise ServerError(response["errors"])

        return response
    
    def _acquire_data(self, timestamp, window_length):
        if timestamp == -1:
            timestamp = self._latest_timestamp

        data = super(BacktestDataHandler, self)._acquire_data(timestamp, window_length)
        
        if timestamp >= self._latest_timestamp:
            data["notices"].append("REQUEST_LATEST_DATA")
            
        return data

    def prepare_dataset(self, scroll, window_length):
        previous_latest_timestamp = self._latest_timestamp
        self._latest_timestamp = self._master.sync(self.__strategy) 
        self._latest_timestamp = (self._latest_timestamp // self._timeframe) * self._timeframe
        
        if self._timestamp_start >= previous_latest_timestamp + self._forward_projection * self._timeframe:
            self._timestamp_start = self._latest_timestamp + self._forward_projection * self._timeframe

        super(BacktestDataHandler, self).prepare_dataset(scroll, window_length)
        
    def get_latest_timestamp(self):
        return self._master.sync(self.__strategy)

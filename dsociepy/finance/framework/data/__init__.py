#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

class DataSet:
    def __init__(self, timeframe):
        self._timeframe = timeframe
        self.reset()

    def yield_data(self):
        start_index, offset_index = self.get_positions(read_data=True)
        data = self._data[offset_index:start_index][::-1]
            
        for d in data:
            yield d

    def clear_horizon(self, forward_projection=0, backward_projection=0):
        forward = self._timestamp + (((2 * self._window_length) + forward_projection) * self._timeframe)
        backward = self._timestamp - (((3 * self._window_length) + backward_projection * 2) * self._timeframe)
        self._data = [ d for d in self._data if d[0] >= backward and d[0] <= forward ]

    def get_positions(self, strict=False, read_data=False):
        try:
            start_index = [d[0] for d in self._data].index(self._timestamp) + 1

        except Exception as e:
            if strict:
                raise e

            else:
               start_index = len(self._data)

        offset_index = start_index - self._window_length + 1    

        if offset_index < 0:
            if start_index != 0 and read_data:
                return (start_index, 0)

            raise ValueError("Missing data.")
            
        return (start_index, offset_index)

    def set_timestamp_and_window_length(self, timestamp, window_length):
        self._timestamp = timestamp
        self._window_length = window_length

    def update(self, frames):
        must_sort = False
        
        for frame in frames:
            try:
                index = [ d[0] for d in self._data].index(frame[0]) # list is sorted anyway, may be optimized
                if self._data[index] != frame and len(frame) > 1:
                    self._updated = True
                    self._data[index] = frame

            except ValueError:
                must_sort = True
                self._data.append(frame)
                self._updated = True

        if must_sort:
            self._data = sorted(self._data, key=lambda d : d[0])

    def is_updated(self):
        updated = self._updated
        self._updated = False
        return updated

    def reset(self):
        self._data = []
        self._updated = False
        self.set_timestamp_and_window_length(0,0)

    def set_feed_callback(self, callback):
        self._feed_callback = callback

    def get_extremums(self, data_set=None):
        if data_set == None:
            data_set = self._data

        try:
            mx = max([ max(frame[1:]) for frame in data_set if len(frame) > 1])
            mn = min([ min([ v for v in frame[1:] if v != -1]+[4294967296]) for frame in data_set if len(frame) > 1])
            
        except ValueError:
            mx = 1
            mn = 0

        if mx == mn:
            mx += 0.01
            mn -= 0.01

        try:
            return (mn, mx, data_set[-1][0], data_set[0][0])

        except Exception as e:
            raise e

    def get_window_extremums(self, strict=False):
        """Return a tuple holding extremums for given window organized as follow: (min price, max price, oldest time, latest time)."""
        
        try:
            start_index, offset_index = self.get_positions()
            window = self._data[offset_index:start_index]

        except Exception as e:
            if strict:
                raise e

            else:
                window = []
                if self._timestamp == -1:
                    raise ValueError

                timestamp_start = self._timestamp
                timestamp_end = self._timestamp - (self._window_length - 1) * self._timeframe
                for d in self._data:
                    if d[0] >= timestamp_end and d[0] <= timestamp_start:
                        window.append(d)
                
        return self.get_extremums(window)

    def yield_timestamps(self):
        for d in self.yield_data(read_data=True):
            yield d[0]

    def yield_values(self):
        for d in self.yield_data(read_data=True):
            yield d[1:]

    def yield_whole_timestamps(self):
        for d in self._data[::-1]:
            yield d[0]
    
    def yield_whole_values(self):
        for d in self._data[::-1]:
            yield d[1:]

    def get_oldest(self):
        try:
            return self._data[0]

        except:
            return None
    
    def get_by_timestamp(self, timestamp):
        index = self.get_index_by_timestamp(timestamp)
        return self._data[index]
        
    def get_index_by_timestamp(self, timestamp):
        return [ d[0] for d in self._data].index((timestamp // self._timeframe)* self._timeframe)

    def get_latest(self):
        try:
            return self._data[-1]

        except:
            return None

    def length(self):
        return len(self._data)

    def get_data(self):
        return self._data

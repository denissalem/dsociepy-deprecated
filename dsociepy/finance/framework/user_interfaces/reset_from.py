#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

from dsociepy.finance.framework.user_interfaces import spin_button_factory 
from dsociepy.finance.framework.user_interfaces import InputWrapper

class ResetFrom(Gtk.Frame):
    def __init__(self, default_timestamp):
        super(ResetFrom, self).__init__()
        self.__wait_reset_from = Gtk.Label("Warming up...")
        self.__wait_reset_from.set_margin_bottom(3)

        # Setting up context
        self.oldest = datetime.datetime.fromtimestamp(1984)
        self.latest = datetime.datetime.fromtimestamp(2984)
        self.__box = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=3
        )
        self.add(self.__box)

        self.__default_timestamp = default_timestamp
        if default_timestamp > self.oldest.timestamp() and default_timestamp < self.latest.timestamp():
            self.default = datetime.datetime.fromtimestamp(self.__default_timestamp)
        
        else:
            self.default = self.oldest

        self.year   = InputWrapper("Year:", spin_button_factory(1984, 2984, self.default.year, self.__value_changed), Gtk.Orientation.HORIZONTAL, False) 
        self.month  = InputWrapper("Month:", spin_button_factory(1, 12, self.default.month, self.__value_changed), Gtk.Orientation.HORIZONTAL, False)
        self.day    = InputWrapper("Day:", spin_button_factory(1, 31, self.default.day, self.__value_changed), Gtk.Orientation.HORIZONTAL, False)
        self.hour   = InputWrapper("Hour:", spin_button_factory(0, 23, self.default.hour, self.__value_changed), Gtk.Orientation.HORIZONTAL, False)
        self.minute = InputWrapper("minute:", spin_button_factory(0, 59, self.default.minute, self.__value_changed), Gtk.Orientation.HORIZONTAL, False)
     
        self.reset = Gtk.Button("Reset")
        self.reset.set_margin_left(3)
        self.reset.set_margin_right(3)
        self.__label = Gtk.Label("Reset backtest from:") 
        self.__box.pack_start(self.__wait_reset_from, True, True, 0)
        self.__box.pack_start(self.__label, True, True, 0)
        self.__box.pack_start(self.year.frame, True, True, 0) 
        self.__box.pack_start(self.month.frame, True, True, 0) 
        self.__box.pack_start(self.day.frame, True, True, 0) 
        self.__box.pack_start(self.hour.frame, True, True, 0) 
        self.__box.pack_start(self.minute.frame, True, True, 0)
        self.__box.pack_start(self.reset, False, False, 3) 
        self.__box.set_margin_top(3)
        
        self.show_all()

        self.__widgets = [
            self.year, self.month, self.day, self.hour, self.minute, self.reset
        ]

    def __check_date_conformity(self):
        try:
            current = datetime.datetime(
                year = self.year.get_input().get_value_as_int(),
                month = self.month.get_input().get_value_as_int(),
                day = self.day.get_input().get_value_as_int(),
                hour = self.hour.get_input().get_value_as_int(),
                minute = self.minute.get_input().get_value_as_int()
            )

            if current < self.oldest:
                return -1
            elif current > self.latest:
                return 1

        except ValueError:
            self.day.get_input().set_value(
                self.day.get_input().get_value_as_int() - 1
            )
            return self.__check_date_conformity()

        return 0

    def __value_changed(self, spin_button):
        c = self.__check_date_conformity()
        fix = None

        if c == -1:
            fix =self.oldest

        elif c == 1:
            fix = self.latest

        if fix != None:
            self.year.get_input().set_value(fix.year)
            self.month.get_input().set_value(fix.month)
            self.day.get_input().set_value(fix.day)
            self.hour.get_input().set_value(fix.hour)
            self.minute.get_input().set_value(fix.minute)

   
    def __set(self):
        self.__wait_reset_from.hide()
        self.__label.show()
        for widget in self.__widgets:
            try:
                widget.show()

            except:
                widget.frame.show()
        self.reset.show()

    def __set_sensitive(self, value):
        for widget in self.__widgets:
            widget.set_sensitive(value)

    def __set_range(self, mx, mn):
        self.oldest = datetime.datetime.fromtimestamp(mn)
        self.latest = datetime.datetime.fromtimestamp(mx)
        
        self.year.get_input().set_range(self.oldest.year, self.latest.year)
        if self.__default_timestamp > self.oldest.timestamp() and self.__default_timestamp < self.latest.timestamp():
            self.default = datetime.datetime.fromtimestamp(self.__default_timestamp)
        
        else:
            self.default = self.oldest

        self.year.get_input().set_value(self.default.year)
        self.month.get_input().set_value(self.default.month)
        self.day.get_input().set_value(self.default.day)    
        self.hour.get_input().set_value(self.default.hour) 
        self.minute.get_input().set_value(self.default.minute)

    def get_value(self):
        return datetime.datetime(
            year=int(self.year.get_input().get_value()),
            month=int(self.month.get_input().get_value()),
            day=int(self.day.get_input().get_value()),
            hour=int(self.hour.get_input().get_value()),
            minute=int(self.minute.get_input().get_value())
        ).timestamp()

    def set_range(self, mx, mn):
        GLib.idle_add(self.__set_range, mx, mn)
    
    def set_sensitive(self, value):
        GLib.idle_add(self.__set_sensitive, value)

    def set(self):
        GLib.idle_add(self.__set)

    def warming_up(self):
        self.__label.hide()
        self.__wait_reset_from.show()
        for widget in self.__widgets:
            try:
                widget.hide()
            
            except:
                widget.frame.hide()

        self.reset.hide()


#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

class SetupMDS(Gtk.Window):
    def __init__(self, servers_list):
        self.servers_list = servers_list
        super(Gtk.Window, self).__init__(title = "Setup market data servers")
        self.set_modal(True)
        self.set_default_size(320,240)
        self.__liststore = Gtk.ListStore(str, int)

        self.__treeview = Gtk.TreeView(model=self.__liststore)
        self.__treeview.connect("key-press-event", self.__delete_row)
        self.__renderer_text_ip = Gtk.CellRendererText()
        self.__renderer_text_ip.set_property("editable", True)
        self.__column_ip = Gtk.TreeViewColumn("IP", self.__renderer_text_ip, text=0)

        self.__renderer_text_port = Gtk.CellRendererText()
        self.__renderer_text_port.set_property("editable", True)
        self.__column_port = Gtk.TreeViewColumn("Port", self.__renderer_text_port, text=1)       
        
        self.__treeview.append_column(self.__column_ip)
        self.__treeview.append_column(self.__column_port)

        self.__renderer_text_ip.connect("edited", self.__ip_edited)
        self.__renderer_text_port.connect("edited", self.__port_edited)

        self.setup_button = Gtk.Button(label="Setup")

        self.__box = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=0
        )

        self.__box.pack_start(self.__treeview, True, True, 3)
        self.__box.pack_start(self.setup_button, False, False, 3)
        
        self.add(self.__box)

        self.show_all()

        self.__init_liststore()

    def __delete_row(self, widget, a):
        if a.hardware_keycode == 119: #suppr
            liststore, path = self.__treeview.get_selection().get_selected_rows()
            ip = liststore[path][0]
            port = liststore[path][1]
            try:
                self.servers_list.pop(self.servers_list.index([ip, port]))
                self.__init_liststore()

            except ValueError:
                pass

            self.__init_liststore()

    def __init_liststore(self):
        self.__liststore.clear()
        for server in self.servers_list:
            self.__liststore.append(server)
        self.__liststore.append(["",6473])
        self.__treeview.set_model(self.__liststore)

    def __liststore_to_servers_list(self):
        self.servers_list = []
        for tvm in self.__liststore:
            if self.__liststore[tvm.path][0] != '':
                self.servers_list.append([
                    self.__liststore[tvm.path][0],
                    self.__liststore[tvm.path][1]
                ])

    def __ip_edited(self, widget, path, text):
        self.__liststore[path][0] = text

        if self.__liststore[path][0] != '':
            self.__liststore_to_servers_list()
            self.__init_liststore()

    def __port_edited(self, widget, path, text):
        try:
            self.__liststore[path][1] = int(text)
            self.__liststore_to_servers_list()
            self.__init_liststore()

        except ValueError:
            pass

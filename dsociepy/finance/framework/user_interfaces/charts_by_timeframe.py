#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

import threading

from dsociepy.finance.charts.price import Price
from dsociepy.finance.framework.data.data_handler import DataHandler
from dsociepy.finance.framework.helpers import RESSOURCE_FOLDER
from dsociepy.finance.framework.helpers import TAB_NAMES_BY_TIMEFRAMES
from dsociepy.finance.framework.volume_control import VolumeControl
from dsociepy.finance.framework.ichimoku_control import IchimokuControl

from dsociepy.finance.framework.user_interfaces.panes_manager import PanesManager
from dsociepy.finance.framework.user_interfaces.indicators_manager import IndicatorsManager

class ChartsByTimeframe:
    def __init__(self, trade_context, timeframe):
        self._thread_buffering = threading.Thread(target=self._sync_buffering, daemon=True)
        self._run = False
        self.__data_handler = trade_context.global_data_handler.add(timeframe)
        self.sync_lock = threading.Lock()
        self.market_name = trade_context.get_mds_context().market
        self.asset_pair = trade_context.get_mds_context().pair
        self.timeframe = timeframe
        self.indicators = []
        self.page = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=0
        )
        self.page.show_all()
        
        self.panes_manager = PanesManager()
        self.menu = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=3
        )

        self.save_chart_button = Gtk.Button()
        self.save_chart_button.set_margin_top(3)
        self.save_chart_button.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconSaveChart.png"))
        self.menu.pack_start(self.save_chart_button, False, False, 0)
 
        # INDICATORDS CONTROLERS
        self.__menu_items = []

        self.volume_controler = VolumeControl(self)
        self.ichimoku_controler = IchimokuControl(self)
        self.__menu_items.append(self.volume_controler.get_menu_item())
        self.__menu_items.append(self.ichimoku_controler.get_menu_item())

        self.indicators_button = IndicatorsManager(self.__menu_items)
        self.menu.pack_start(self.indicators_button, False, False ,0)
        
        self.chartism_button = Gtk.Button()
        self.chartism_button.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconChartism.png"))
        self.menu.pack_start(self.chartism_button, False, False ,0)
        
        self.orders_button = Gtk.Button()
        self.orders_button.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconOrders.png"))
        self.menu.pack_start(self.orders_button, False, False ,0)

        self.page.pack_start(self.menu, False, False, 3)
        self.page.pack_start(self.panes_manager.get_root(), True, True, 0)

        self.tab = Gtk.HBox()
        self.close = Gtk.Button()
        self.close.set_relief(Gtk.ReliefStyle.NONE)
        self.close.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconCloseSmall.png"))
        
        self.tab.pack_start(Gtk.Label(TAB_NAMES_BY_TIMEFRAMES[timeframe]), True, True, 3)
        self.tab.pack_start(self.close, True, True, 0)
        self.tab.show_all()

    def _buffering_thread_handler(self):
        if not self._thread_buffering.isAlive() and self._run:
            self._thread_buffering = threading.Thread(target=self._sync_buffering, daemon=True)
            self._thread_buffering.start()

        return self._run

    def _sync_buffering(self):
        self._set_write_buffering_params(False)
        master_indicator, scroll, window_length = self.get_master_buffering_params()
        master_indicator.prepare_dataset(scroll, window_length)
        
        self._finalize_buffering()
        self._set_write_buffering_params(True)
 
    def _set_write_buffering_params(self, value):
        self.sync_lock.acquire()
        for indicator in self.indicators:
            indicator.set_write_buffering_params(value)
        self.sync_lock.release()

    def _finalize_buffering(self):
        self.sync_lock.acquire()
        for indicator in self.indicators:
            indicator.finalize_buffering()
        self.sync_lock.release()

    def get_data_handler(self):
        return self.__data_handler

    def get_tab_name(self):
        return TAB_NAMES_BY_TIMEFRAMES[self.timeframe]

    def set_price_indicators(self, auto_refresh_timeout):
        self._price_indicators = Price(self.__data_handler)
        self._price_indicators.set_sync_lock(self.sync_lock)
        self._price_indicators.set_title('') # must be defined by user
        self._price_indicators.set_subtitle(self.market_name+', '+self.asset_pair)
        self._price_indicators.auto_refresh_timeout(auto_refresh_timeout)
        self.indicators.append(self._price_indicators)
        self.panes_manager.add(self._price_indicators)
    
    def get_data_handler(self):
        return self.__data_handler

    def get_ui(self):
        return (self.page, self.tab)

    def stop_all_widgets(self):
        self._run = False
        if self._callback_buffering_thread_handler != -1:
            GLib.source_remove(self._callback_buffering_thread_handler)
            self._callback_buffering_thread_handler = -1

        try:
            self._thread_buffering.join()
            
        except RuntimeError:
            pass

    def start_all_widgets(self):
        self._run = True
        self._callback_buffering_thread_handler = GLib.timeout_add(60, self._buffering_thread_handler)
   
    def get_master_buffering_params(self):
        self.sync_lock.acquire()
        master_time = 0
        master_indicator = None
        for indicator in self.indicators:
            if master_time < indicator.get_master():
                master_time = indicator.get_master()
                master_indicator = indicator
        
        scroll, window_length = master_indicator.get_buffering_params()
        self.sync_lock.release()
        return (master_indicator, scroll, window_length)



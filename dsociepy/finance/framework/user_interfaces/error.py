#! /usr/bin/python3

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class Error(Gtk.Window):
    def __init__(self, Errors):
        super(Gtk.Window, self).__init__(title = "Errors")
        self.set_modal(True)
        self.boxes = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=6
        )
        self.boxes.set_margin_left(6)
        self.boxes.set_margin_right(6)
        self.button = Gtk.Button.new_with_label("Fuck it, I will fix that")
        self.button.connect("clicked", self.close)
        for error in Errors:
            self.boxes.pack_start(Gtk.Label(error), False, False, 6)
        
        self.boxes.pack_start(self.button, False, False, 6)
        
        self.add(self.boxes)
        self.connect("delete-event", self.close)
        self.show_all()


    def close(self, widget, event=None):
        self.destroy()

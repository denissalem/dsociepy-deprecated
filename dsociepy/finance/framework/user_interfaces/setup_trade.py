#! /usr/bin/python3

import gi

import threading

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

class SetupTrade(Gtk.Window):
    def __init__(self, get_asset_pairs):
        super(Gtk.Window, self).__init__(title = "Setup trade")
        self.__thread = threading.Thread()
        self.get_asset_pairs = get_asset_pairs
        self.set_modal(True)
        self.boxes = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=3
        )  
        self.warming_up = Gtk.Label("Retrieving asset pairs...")
        self.button_setup_backtest = Gtk.Button(label="Setup backtest")
        self.button_setup = Gtk.Button(label="Setup real trade")
        self.set_warming_up(True)
        GLib.idle_add(self.do_warming_up)
        
        self.boxes.set_margin_left(3)
        self.boxes.set_margin_right(3)

        self.boxes.pack_start(self.warming_up, False, False, 0)
        
        self.add(self.boxes)
        self.connect("delete-event", self.close)
        self.show_all()
    
    def do_warming_up(self):
        if not self.__thread.isAlive():
            self.__thread = threading.Thread(target=self.setup_ui, daemon=True)
            self.__thread.start()

    def setup_ui(self):
        self.asset_pairs = Gtk.ListStore(str, str)
        for asset_pair in sorted(self.get_asset_pairs()):
            self.asset_pairs.append([asset_pair, asset_pair])
        
        self.asset_pairs_combo = Gtk.ComboBox.new_with_model_and_entry(self.asset_pairs)
        self.asset_pairs_combo.set_entry_text_column(1)
        self.asset_pairs_combo.connect("changed", self.pick_asset_pair)
        
        # PACKING AND FINAL SETUP
        self.asset_pairs_combo.set_margin_top(3)
        self.button_setup.set_margin_bottom(3)
        GLib.idle_add(self.set_warming_up,False)

    def set_warming_up(self, warming_up):
        if warming_up:
            self.warming_up.show()
        
        else:
            self.boxes.pack_start(self.asset_pairs_combo, False, False, 0)
            self.boxes.pack_start(self.button_setup_backtest, False, False, 0)
            self.boxes.pack_start(self.button_setup, False, False, 0)
            self.show_all()
            self.warming_up.hide()

    def pick_asset_pair(self, combo):
        index = combo.get_active()
        model = combo.get_model()
        self.selected_asset_pair = model[index][0]
        if self.selected_asset_pair != None:
            self.button_setup.set_sensitive(True)

        else:
            self.button_setup.set_sensitive(False)

    def close(self, widget, event):
        self.destroy()

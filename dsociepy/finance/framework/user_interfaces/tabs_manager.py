#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

class TabsManager:
    def __init__(self, backends, parent_context_name):
        self.__backends = backends
        self.__parent_context_name = parent_context_name
        self.__trade_windows = list()

    def detach_tab(self, notebook, page, x, y):
        tab_name = ''
        for trade in self.__backends:
            p, ui = trade.get_ui()
            if p == page:
                tab_name = trade.get_tab_name()
                break
        
        context = (Gtk.Window(title=self.__parent_context_name+', '+tab_name), notebook, page, notebook.get_tab_label(page))
        self.__trade_windows.append(context)
        self.__create_window(context)

    def __create_window(self, context):
        context[1].detach_tab(context[2])
        context[0].set_default_size(1024, 640)
        context[0].connect("delete-event", self.__close_trade_window)
        context[0].add(context[2])
        context[0].show_all()

    def __close_trade_window(self, widget, event):
        index = [d[0] for d in self.__trade_windows].index(widget)
        
        window, notebook, tab, label = self.__trade_windows[index]

        tab.get_parent().remove(tab)

        notebook.insert_page(tab, label, -1)
        notebook.set_tab_detachable(tab, True)
        notebook.set_tab_reorderable(tab, True)
        notebook.set_current_page(-1)
        self.__trade_windows.pop(index)

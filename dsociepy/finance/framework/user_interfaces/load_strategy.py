#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class LoadStrategy(Gtk.Frame):
    def __init__(self):
        super(LoadStrategy, self).__init__()
        self.__box = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=3
        )
        self.__strategy = Field("Load strategy:", "")
        self.__box.pack_start(self.__strategy.frame, True, True, 0)
        self.add(self.__box)
        


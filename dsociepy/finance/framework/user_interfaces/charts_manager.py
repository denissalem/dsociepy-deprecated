#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from dsociepy.finance.framework.user_interfaces import DropDown
from dsociepy.finance.framework.helpers import RESSOURCE_FOLDER
from dsociepy.finance.framework.helpers import TAB_NAMES_BY_TIMEFRAMES

class ChartsManager(DropDown):
    def __init__(self, charts, add_chart):
        self.__values = [ TAB_NAMES_BY_TIMEFRAMES[k] for k in sorted(TAB_NAMES_BY_TIMEFRAMES.keys())]
        self.__charts = charts
        self.__add_chart = add_chart
        super(ChartsManager, self).__init__(
            Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconAddChart.png"),
            self.__values
        )

        self.connect("clicked", self.__set_menu)

    def __set_menu(self, widget):
        for item in self._menu:
            self._menu.remove(item)

        labels = []
        for chart in self.__charts:
            labels.append(TAB_NAMES_BY_TIMEFRAMES[chart.timeframe])

        for value in self.__values:
            if not value in labels:
                item = Gtk.MenuItem()
                item.set_label(str(value))
                item.connect("activate", self.__add_chart)
                item.show()
                self._menu.append(item)


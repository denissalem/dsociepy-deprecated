#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class IndicatorControl(Gtk.MenuItem):
    def __init__(self, parent, label):
        self._parent = parent
        self._label = label
        super(IndicatorControl, self).__init__()
        self.set_label(self._label)
        self.connect("activate", self._clicked)

    def _clicked(self, widget):
        label = self.get_children()[0]
        if self._parent.display == False:
            label.set_markup("<b>"+self._label+"</b>")
            self._parent.display = True
            self._parent.add()
            
        else:
            label.set_markup(self._label)
            self._parent.display = False
            self._parent.remove()

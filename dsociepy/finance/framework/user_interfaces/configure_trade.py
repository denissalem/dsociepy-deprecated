#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

class ConfigureTrade:
    def __init__(self):
        self.save_config_button = Gtk.Button("Save/Reload")
        self.save_config_button.set_margin_left(3)
        self.save_config_button.set_margin_right(3)
        
        self.page = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=0
        )
        self.page.set_events(Gdk.EventMask.SCROLL_MASK)
        #self.page.grab_add()
        self.options = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=0
        )
        self.options.set_margin_top(3)
        self.options.set_margin_bottom(3)

        self.page.pack_start(self.options, False, False, 0)

        self.left = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=0
        )
        self.center_left = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=0
        )
        self.center_right = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=0
        )
        self.right = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=0
        )
        self.right.set_margin_right(3)
        self.right.set_margin_left(3)

        self.options.pack_start(self.left, True, True, 0)
        self.options.pack_start(self.center_left, True, True, 0)
        self.options.pack_start(self.center_right, True, True, 0)
        self.options.pack_start(self.right, True, True, 0)

        self.page.pack_start(self.save_config_button, False, False, 0)

    def pack_left(self, form):
        self.left.pack_start(form, False, False, 0)
    
    def pack_center_left(self, form):
        self.center_left.pack_start(form, False, False, 0)
    
    def pack_center_right(self, form):
        self.center_right.pack_start(form, False, False, 0)
    
    def pack_right(self, form):
        self.right.pack_start(form, False, False, 0)


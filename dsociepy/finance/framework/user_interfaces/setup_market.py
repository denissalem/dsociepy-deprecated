#! /usr/bin/python3

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class SetupMarket(Gtk.Window):
    def __init__(self, fields):
        # WINDOW CONFIGURATION
        super(Gtk.Window, self).__init__(title = "Setup Market")
        self.set_modal(True)
        
        # FIELDS
        self.boxes = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=3
        )
        self.boxes.set_margin_left(3)
        self.boxes.set_margin_right(3)

        fields[0].frame.set_margin_top(3)
        for field in fields:
            self.boxes.pack_start(field.frame, False, False, 0) 

        # BUTTON
        self.button = Gtk.Button.new_with_label("Setup")
        self.button.set_margin_bottom(3)
        self.boxes.pack_start(self.button, False, False, 0) 
    
        # FINAL CONFIGURATION
        self.add(self.boxes)
        self.connect("delete-event", self.close)
        self.show_all()

    def close(self, widget, event=None):
        self.destroy()

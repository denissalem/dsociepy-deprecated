#! /usr/bin/python3

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class SaveMarketData:
    def __init__(self, parent_context):
        self.__parent_context = parent_context
        self.__dialog = Gtk.FileChooserDialog(
            "Save as JSON market file", 
            self.__parent_context.market_handler.parent_context.user_interface,
            Gtk.FileChooserAction.SAVE,
            (
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_SAVE, Gtk.ResponseType.OK
            )
        )
        self.response = self.__dialog.run()

    def get_filename(self):
        return self.__dialog.get_filename()

    def close(self):
        self.__dialog.destroy() 

#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from dsociepy.finance.framework.helpers import RESSOURCE_FOLDER

class MainWindow(Gtk.Window):
    def __init__(self, parent_context):
        self.parent_context = parent_context
        Gtk.Window.__init__(self, title="DSOCIEPY Finance Framework")
        self.set_default_size(1024, 640)

        # TOP LEVEL BOXES
        self.top_level_boxes = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=0
        )

        self.add(self.top_level_boxes)
        
        # MAIN MENU
        self.main_menu = Gtk.Box(
            spacing=0
        )
        self.top_level_boxes.pack_start(self.main_menu, False, False, 3)

        # MAIN MENU BUTTONS
        self.join_market = Gtk.Button()
        self.join_market.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconMarketPlace.png"))
        self.setup_mds = Gtk.Button()
        self.setup_mds.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconServer.png"))
        self.overview = Gtk.Button()
        self.overview.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconOverview.png"))
        self.run_markets = Gtk.Button()
        self.run_markets.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconRun.png"))
        self.pause_markets = Gtk.Button()
        self.pause_markets.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconPause.png"))
        self.main_menu.pack_start(self.join_market, False, False, 3)
        self.main_menu.pack_start(self.setup_mds, False, False, 0)
        self.main_menu.pack_start(self.overview, False, False, 3)
        self.main_menu.pack_start(self.run_markets, False, False, 0)
        self.main_menu.pack_start(self.pause_markets, False, False, 3)
        
        # MARKET NOTEBOOK
        self.markets = Gtk.Notebook()
        self.markets.connect("switch-page", self.__switch_page)
        self.markets.set_scrollable(True)
        self.top_level_boxes.pack_start(self.markets, True, True, 0)

    def __switch_page(self, notebook, page, page_num):
        i = 0
        for market in self.parent_context.markets:
            if i == page_num:
                trade = market.user_interface.trades.get_current_page()
                if trade != -1:
                    if market.trades[trade].user_interface.overview.get_current_page() == 0:
                        market.trades[trade].charts.start_all_widgets()

                    else:
                        market.trades[trade].charts.stop_all_widgets()
                
            else:
                for trade in market.trades:
                    index = market.user_interface.trades.page_num(trade.user_interface.page)
                    if index != -1:
                        trade.charts.stop_all_widgets()
            
            i+=1

# https://stackoverflow.com/questions/30705246/how-to-create-a-dropdown-from-a-gtk-entrys-icon
class DropDown(Gtk.MenuButton):
    def __init__(self, label, values):
        if type(label) == str:
            super(DropDown, self).__init__(label)
        else:
            super(DropDown, self).__init__()
            self.set_image(label)
        self._menu = Gtk.Menu()
        self.set_popup(self._menu)
        self.set_direction(Gtk.ArrowType.DOWN)

        for value in values:
            if type(value) == str:
                item = Gtk.MenuItem()
                item.set_label(str(value))

            else:
                item = value

            item.show()
            self._menu.append(item)

class Setter:
    def __init__(self, button_label, value, setter):
        self.setter = setter
        self.frame = Gtk.Frame()
        self.box = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=3
        )
        self.box.set_margin_top(3)
        self.box.set_margin_bottom(3)
        
        self.frame.add(self.box)

        self.entry = Gtk.Entry()
        self.entry.set_text(str(value))
        self.entry.set_width_chars(6)
        
        self.button = Gtk.Button(label=button_label)
        self.button.connect("clicked", self.setter)

        self.box.pack_start(self.button, False, False, 0)
        self.box.pack_start(self.entry, False, False, 0)
        self.box.set_margin_right(3)
        self.box.set_margin_left(3)

    def setter(self, widget):
        self.setter(self.entry)

class InputWrapper:
    def __init__(self, label, base_input, orientation=Gtk.Orientation.VERTICAL, enable_frame=True):
        if type(label) == str:
            self.__label = Gtk.Label(label)
        else:
            self.__label = label
        self.name = self.__label.get_text()
        self._base_input = base_input

        if enable_frame:
            self.frame = Gtk.Frame()
        else:
            self.frame = Gtk.Box()

        self.__box = Gtk.Box(
            orientation=orientation,
            spacing=3
        )

        if orientation == Gtk.Orientation.VERTICAL:
            self.__box.set_margin_top(3)
            self.__box.set_margin_bottom(0)
            base_input.set_margin_left(3)
            base_input.set_margin_right(3)
        else:
            self.__box.set_margin_left(3)
            self.__box.set_margin_right(3)
            base_input.set_margin_top(3)
            base_input.set_margin_bottom(3)

        if enable_frame:
            self.frame.add(self.__box)
        else:
            self.frame.pack_start(self.__box, True, True, 0)

        self.__box.pack_start(self.__label, False, False, 3)
        self.__box.pack_start(self._base_input, True, True, 3)

    def set_sensitive(self, value):
        self._base_input.set_sensitive(value)

    def get_input(self):
        return self._base_input

class Combo(InputWrapper):
    def __init__(self, name, values):
        self.__list_store = Gtk.ListStore(str, str)
        for value in values:
            self.__list_store.append([value, value])
        self.__combo = Gtk.ComboBox.new_with_model_and_entry(self.__list_store)
        self.__combo.set_entry_text_column(0)
        self.__combo.connect("changed", self.__changed) 
        self.__value = ""
        
        super(Combo, self).__init__(Gtk.Label(name), self.__combo)

    def __changed(self, combo):
        index = combo.get_active()
        model = combo.get_model()
        self.__value = model[index][1]

        if None == self.__value:
            self.__value = ""

    def get_value(self):
        return self.__value
        
    def set_value(self, value):
        model = self.__combo.get_model()
        for i in range(0,len(model)):
            if model[i][1] == value:
                self.__combo.set_active(i)
                break

    def update(self, values):
        self.__list_store = Gtk.ListStore(str, str)
        for value in values:
            self.__list_store.append([value, value])
        self.__combo.set_model(self.__list_store)

class Field(InputWrapper):
    def __init__(self, name, value, orientation=Gtk.Orientation.VERTICAL, enable_frame=True):
        entry = Gtk.Entry()
        entry.set_text(str(value))
        super(Field, self).__init__(Gtk.Label(name), entry, orientation, enable_frame)

    def get_value(self):
        return self._base_input.get_text()

class Switch:
    def __init__(self, label, context, attr_name):
        self.frame = Gtk.Frame()
        self.label = label
        self.context = context
        self.attr_name = attr_name
        self.box = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=3
        )

        self.box.set_margin_top(3)
        self.box.set_margin_bottom(3)
        self.frame.add(self.box)
        
        self.switch = Gtk.Switch()
        self.switch.set_active(self.context[self.attr_name])
        self.box.pack_start(Gtk.Label(self.label), False, False, 3)
        self.box.pack_start(self.switch, True, True, 3)
        self.switch.connect("notify::active", self.update)

    def update(self, widget, gparam):
        self.context[self.attr_name] = widget.get_active()
        self.context.save_as_json()

def spin_button_factory(mn, mx, value, value_changed_callback):
    spin_button = Gtk.SpinButton.new_with_range(mn,mx,1)
    spin_button.set_numeric(True)
    spin_button.set_margin_left(3)
    spin_button.set_margin_right(3)
    spin_button.set_value(value)
    spin_button.connect("value-changed", value_changed_callback)
    spin_button.update()
    return spin_button


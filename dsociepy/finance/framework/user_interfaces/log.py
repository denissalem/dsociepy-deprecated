#! /usr/bin/python3

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class Log:
    def __init__(self):
        self.page = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=0
        )
        self.name = Gtk.Label("Log")

        self.sw = Gtk.ScrolledWindow()
        self.sw.set_hexpand(True)
        self.sw.set_vexpand(True)
        
        self.text_view = Gtk.TextView()
        self.text_view.set_editable(False)
        
        self.sw.add(self.text_view)
        self.page.pack_start(self.sw, True, True, 0)

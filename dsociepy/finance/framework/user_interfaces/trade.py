#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from dsociepy.finance.framework.user_interfaces import Setter
from dsociepy.finance.framework.helpers import RESSOURCE_FOLDER

class Trade:
    def __init__(self, parent_context):
        self.switch_page_disabled = True
        self.parent_context = parent_context

        # TOP LEVEL BOXES
        self.page = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=0
        )

        self.tab = Gtk.HBox()
        self.close = Gtk.Button()
        self.close.set_relief(Gtk.ReliefStyle.NONE)
        self.close.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconCloseSmall.png"))
        
        if self.parent_context.is_backtest():
            self.tab.pack_start(Gtk.Label('('+self.parent_context.asset_pair+')'), True, True, 3)
        
        else:
            self.tab.pack_start(Gtk.Label(self.parent_context.asset_pair), True, True, 3)
        
        self.tab.pack_start(self.close, True, True, 0)
        self.tab.show_all()
        
        self.close.connect("clicked", self.parent_context.close)

        # OVERVIEW
        self.overview = Gtk.Notebook()
        self.overview.set_margin_left(3)
        self.overview.set_margin_right(3)
        self.overview.set_scrollable(True)
        self.overview.connect("switch-page", self.__switch_page)

        # PACKING 
        self.page.pack_start(self.overview, True, True, 3)

        self.page.show_all()

    def __switch_page(self, notebook, page, page_num):
        if self.switch_page_disabled:
            return

        if page_num != 0:
            self.parent_context.charts.stop_all_widgets()

        else:
            self.parent_context.charts.start_all_widgets()

        if page_num == 4:
            self.parent_context.configure.setup_tab()
        



#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from dsociepy.finance.framework.user_interfaces import Combo
import threading

class PickMDS(Gtk.Box):
    def __init__(self, servers, default_server):
        super(PickMDS, self).__init__(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=0
        )
        self.__servers = Combo("Market Data Server:", servers)
        self.__servers.set_value(default_server)
        self.set_margin_left(3)
        self.set_margin_right(3)
        self.__wait_configure_mds = Gtk.Frame()
        self.__wait_configure_mds_label = Gtk.Label("Warming up...")
        self.__wait_configure_mds_label.set_margin_top(3)
        self.__wait_configure_mds_label.set_margin_bottom(3)
        self.__wait_configure_mds.add(self.__wait_configure_mds_label)
        self.pack_start(self.__wait_configure_mds, False, False, 0)
        self.pack_start(self.__servers.frame, False, False, 0)
    
    def warming_up(self):
        self.__wait_configure_mds.show()
        self.__servers.frame.hide()
    
    def set(self):
        self.__wait_configure_mds.hide()
        self.__servers.frame.show()
        
    def setup_servers(self, servers, default_server):
        self.__servers.update(servers)
        self.__servers.set_value(default_server)

    def get_values(self):
        return self.__servers.get_value()

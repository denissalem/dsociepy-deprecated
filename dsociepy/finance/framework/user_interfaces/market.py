#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
import os

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

from dsociepy.finance.framework.helpers import RESSOURCE_FOLDER
from dsociepy.finance.framework.user_interfaces.tabs_manager import TabsManager

class Market:
    def __init__(self, parent_context):
        self.parent_context = parent_context
        self.icon = Gtk.Image.new_from_file(os.path.expanduser("~")+"/.local/share/dsociepy/ressources/finance/Logo"+parent_context.name.replace(' ','')+".png")
        self.tab = Gtk.HBox()
        self.quit = Gtk.Button()
        self.quit.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconCloseSmall.png"))
        self.quit.set_relief(Gtk.ReliefStyle.NONE)
        self.tab.pack_start(self.icon, True, True, 3)
        self.tab.pack_start(self.quit, True, True, 0)
        self.tab.show_all()
        # TOP LEVEL BOXES
        self.page = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=3
        )

        self.page.set_border_width(3)
        
        # MAIN MENU
        self.main_menu = Gtk.Box(
            spacing=0
        )
        self.page.pack_start(self.main_menu, False, False, 0)

        # MAIN MENU BUTTONS
        self.setup = Gtk.Button()
        self.setup.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconSetup.png"))
        self.trade = Gtk.Button()
        self.trade.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconTrade.png"))
        self.run_trades = Gtk.Button()
        self.run_trades.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconRun.png"))
        self.pause_trades = Gtk.Button()
        self.pause_trades.set_image(Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconPause.png"))
        
        self.main_menu.pack_start(self.setup, False, False, 0)
        self.main_menu.pack_start(self.trade, False, False, 3)
        self.main_menu.pack_start(self.run_trades, False, False, 0)
        self.main_menu.pack_start(self.pause_trades, False, False, 3)

        self.__tabs_manager = TabsManager(self.parent_context.trades, self.parent_context.name)

        # TRADES
        self.trades = Gtk.Notebook()
        self.trades.set_group_name(self.parent_context.name)
        self.trades.connect("switch-page", self.__switch_page)
        self.trades.connect("create-window", self.__tabs_manager.detach_tab)
        self.trades.set_scrollable(True)

        self.page.pack_start(self.trades, True, True, 0)
        
    def append_trade(self, page, tab):
        self.trades.append_page(
            page,
            tab
        )
        self.trades.set_tab_detachable(page, True)
        self.trades.set_tab_reorderable(page, True)
        self.page.show_all()


    def __switch_page(self, notebook, page, page_num):
        for trade in self.parent_context.trades:
            index = self.trades.page_num(trade.user_interface.page)
            if -1 == index:
                trade.charts.start_all_widgets()
            
            else:
                if index == page_num and trade.user_interface.overview.get_current_page() == 0:
                    trade.charts.start_all_widgets()
                
                else:
                    trade.charts.stop_all_widgets()

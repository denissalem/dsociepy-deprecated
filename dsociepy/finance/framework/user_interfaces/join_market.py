#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class JoinMarket(Gtk.Window):
    def __init__(self, parent_context):
        self.market_names= [
            #"BITFINEX",
            #"BITTREX",
            #"BITSTAMP",
            #"BLEUTRADE",
            #"C-CEX",
            #"CEX·IO",
            #"coinbase",
            #"EXMO",
            #"HitBTC",
            "IG Group",
            "Kraken",
            #"KUNA",
            #"LIVECOIN",
            #"LocalBitcoins",
            "POLONIEX"#,
            #"QRYPTOS",
            #"QUOINE",
            #"YObit"
        ]
        
        self.parent_context = parent_context
        self.market = None
        
        # MAKE WINDOW MODAL
        super(Gtk.Window, self).__init__(title = "Join Market")
        self.set_modal(True)

        # TOP LEVEL BOXES
        self.boxes = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=3
        )
        
        # SETUP COMBO
        self.markets = Gtk.ListStore(str, str)

        for market_name in self.market_names:
            if not market_name in self.parent_context.states["markets"].keys():
                self.markets.append([market_name, market_name])

        self.markets_combo = Gtk.ComboBox.new_with_model_and_entry(self.markets)
        self.markets_combo.set_entry_text_column(1)
        self.markets_combo.set_margin_left(3)
        self.markets_combo.set_margin_right(3)
        self.markets_combo.set_margin_top(3)
        self.markets_combo.connect("changed", self.pick_market)
        self.boxes.pack_start(self.markets_combo, False, False, 0)
        self.add(self.boxes)

        # SETUP BUTTON
        self.button = Gtk.Button.new_with_label("Join")
        self.button.set_sensitive(False)
        self.button.set_margin_left(3)
        self.button.set_margin_right(3)
        self.button.set_margin_bottom(3)
        self.button.connect("clicked", self.join)
        self.boxes.pack_start(self.button, False, False, 0)

        # WAIT FOR USER INPUT
        self.show_all()
        self.connect("delete-event", self.close)

    def join(self, widget):
        if self.market != None: 
            self.destroy()
            self.parent_context.append_market(self.market)
    
    def pick_market(self, combo):
        index = combo.get_active()
        model = combo.get_model()
        self.market = self.markets[index][0]
        self.button.set_sensitive(True)

    def close(self, widget, event=None):
        self.destroy()

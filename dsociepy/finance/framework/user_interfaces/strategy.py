#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class BaseStrategy(Gtk.Box):
    def __init__(self, orientation_vertical=True):
        if orientation_vertical:
            orientation = Gtk.Orientation.VERTICAL

        else:
            orientation = Gtk.Orientation.HORIZONTAL

        super(BaseStrategy, self).__init__(
            orientation=orientation,
            spacing=0
        )
        self._name = Gtk.Label("Strategy")
        self._reset_button = Gtk.Button("Reset")
        self._start_button = Gtk.Button("Start")
        self._stop_button = Gtk.Button("Stop")

    def set_default(self):
        self.pack_start(Gtk.Label("Default Strategy does nothing! Write your own!"), True, True, 0)

    def get_ui(self):
        return (self, self._name)

    def get_reset_button(self):
        return self._reset_button

    def get_start_button(self):
        return self._start_button

    def get_stop_button(self):
        return self._stop_button

    def new_label(self, text):
        return Gtk.Label(text)

    def new_box(self, vertical=True, spacing=0):
        if vertical:
            return Gtk.Box(Gtk.Orientation.VERTICAL, spacing)

        return Gtk.Box(Gtk.Orientation.HORIZONTAL, spacing)

#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from dsociepy.finance.framework.user_interfaces import DropDown
from dsociepy.finance.framework.helpers import RESSOURCE_FOLDER

class IndicatorsManager(DropDown):
    def __init__(self, menu_items):
        self.__indicators = menu_items
        super(IndicatorsManager, self).__init__(
            Gtk.Image.new_from_file(RESSOURCE_FOLDER+"/IconIndicators.png"),
            self.__indicators
        )

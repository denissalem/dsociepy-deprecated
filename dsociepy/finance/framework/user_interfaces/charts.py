#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class Charts:
    def __init__(self, charts_manager):
        self.charts = Gtk.Notebook()
        self.charts.set_scrollable(True)
        self.charts.set_margin_left(3)
        self.charts.set_margin_right(3)
        
        self.page = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=0
        )
        self.__menu = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=0
        )
        charts_manager.set_margin_top(3)
        self.page.pack_start(self.__menu, False, False, 0)
        self.page.pack_start(self.charts, True, True, 3)
        
        self.tab = Gtk.HBox()
        self.tab.pack_start(Gtk.Label("Charts"), True, True, 3)
        self.tab.pack_start(charts_manager, True, True, 0)
        self.tab.show_all()

    def remove_page(self, page):
        page_num = self.charts.page_num(page)
        self.charts.remove_page(page_num)
        
    def get_ui(self):
        return (self.page, self.tab)


#! /usr/bin/python3

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class AreYouSure:
    def __init__(self, parent, message):
        self.parent = parent
        self.window = Gtk.MessageDialog(
            self.parent,
            Gtk.DialogFlags.MODAL,
            Gtk.MessageType.QUESTION,
            Gtk.ButtonsType.YES_NO,
            message
        )
        self.window.show_all()
        self.response = self.window.run()
        self.window.destroy()

#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

# https://stackoverflow.com/questions/27343166/gtk3-replace-child-widget-with-another-widget

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class PanesManager:
    def __init__(self):
        self.__root_box = Gtk.Box()
        self.__childs = []
        self.__panes = []
        self.__reset()

    def __reset(self):
        for pane in self.__panes[::-1]:
            pane.destroy()

        self.__panes = []
        self.__panes.append(Gtk.VPaned())
        self.__root_box.pack_start(self.__panes[0], True, True, 0)
        self.__root_box.show_all()

    def __rebuild_panes(self):
        pane = self.__panes[0]
        height = self.__root_box.get_allocation().height / len(self.__childs)
        for child in self.__childs:
            pane.pack1(child, True, True)
            if child != self.__childs[-1]:
                self.__panes.append(Gtk.VPaned())
                pane.pack2(self.__panes[-1], True, True)
                pane = self.__panes[-1]

            child.connect_event()
            if child != self.__childs[-1]:
                child.set_draw_ruler_x(False)
            else:
                child.set_draw_ruler_x(True)

        for i in range(0, len(self.__panes)-1):
            self.__panes[i].set_position(int(height))
        
        self.__root_box.show_all()

    def add(self, child):
        self.__reset()
        self.__childs.append(child)
        self.__rebuild_panes()

    def remove(self, child):
        self.__reset()
        self.__childs.remove(child)
        self.__rebuild_panes()

    def get_root(self):
        return self.__root_box

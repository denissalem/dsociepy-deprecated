#! /usr/bin/python3

import time
from dsociepy.finance.framework.user_interfaces.setup_trade import SetupTrade as UserInterface

class SetupTrade:
    def __init__(self, market_handler):
        self.market_handler = market_handler
        self.states = market_handler.states
        self.user_interface = UserInterface(self.market_handler.market_interface.get_asset_pairs)
        self.user_interface.button_setup.connect("clicked", self.setup)
        self.user_interface.button_setup_backtest.connect("clicked", self.setup_backtest)

    def setup_backtest(self, widget):
        self.setup(widget, True)

    def setup(self, widget, backtest=False):
        if self.user_interface.selected_asset_pair != None:
            try:
                setup = self.states["markets"][self.market_handler.name]

                trade_id = str(time.time())
                self.states["markets"][self.market_handler.name]["trades"][trade_id] = {
                    "assetPair" : self.user_interface.selected_asset_pair,
                    "backtest" : backtest,
                    "marketDataServer" : ""
                }
                self.market_handler.append_trade(
                    self.user_interface.selected_asset_pair,
                    trade_id,
                    backtest
                )

                self.states.save_as_json()
                self.user_interface.destroy()

            except ValueError as e:
                Error(["Not a JSON file"])

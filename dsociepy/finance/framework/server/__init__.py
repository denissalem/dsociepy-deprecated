#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import os
import json
import threading

from http.server import HTTPServer

from dsociepy.finance.framework.data.recorder import Recorder
from dsociepy.finance.framework.data.integrity import check_datastore_timeframes
from dsociepy.finance.framework.data.integrity import restore_timeframe
from dsociepy.finance.framework.helpers import MARKET_DATA_FOLDER
from dsociepy.finance.framework.helpers import TIMEFRAMES
from dsociepy.finance.framework.market_interfaces import MARKET_INTERFACES
from dsociepy.finance.framework.log import Log
from dsociepy.finance.framework.server.end_points import EndPointOHLCV
from dsociepy.finance.framework.server.end_points import end_point_pairs
from dsociepy.finance.framework.server.end_points import end_point_oldest_ohlcv_window
from dsociepy.finance.framework.server.exceptions import NoEndPoint
from dsociepy.finance.framework.server.exceptions import UnknownEndPoint
from dsociepy.finance.framework.server.exceptions import TooMuchArgs
from dsociepy.finance.framework.server.exceptions import MissingOrWrongArgs
from dsociepy.finance.framework.server.exceptions import WrongArgsType
from dsociepy.finance.framework.server.rest_interface import RestInterface

VALIDATION = {
    "ohlcv" : ["market", "pair", "timestamp","window","timeframe"],
    "oldest_ohlcv_window" : ["market", "pair", "window"],
    "pairs" : []
}

ARGS_TYPE = {
    "market" : str,
    "pair" : str,
    "timestamp" : int,
    "window" : int,
    "timeframe": int
}

END_POINTS = {
    "pairs" : end_point_pairs, 
    "oldest_ohlcv_window" : end_point_oldest_ohlcv_window
}

def validate(end_point, args):
    if end_point == '':
        raise NoEndPoint
    
    if not end_point in VALIDATION.keys():
        raise UnknownEndPoint

    if len(args.keys()) > len(VALIDATION[end_point]):
        raise TooMuchArgs

    for arg in VALIDATION[end_point]:
        if not arg in args.keys():
            raise MissingOrWrongArgs
    
    for arg in VALIDATION[end_point]:
        try:
            ARGS_TYPE[arg](args[arg])

        except ValueError:
            raise WrongArgsType

class Server(HTTPServer):
    def __init__(self,):
        super(HTTPServer, self).__init__(('', 6473), RestInterface)

        os.makedirs(MARKET_DATA_FOLDER, exist_ok=True)
        self.__log = Log(with_ui=False, output_log="MDS-%Y-%m")
        self.__locks = dict()

        self.__recorders = dict()
        for market_name in os.listdir(MARKET_DATA_FOLDER):
            try:
                self.__recorders[market_name] = Recorder(
                    os.listdir(MARKET_DATA_FOLDER+'/'+market_name),
                    market_name,
                    MARKET_INTERFACES[market_name],
                    self.__log.update,
                    self.__locks
                )
            except KeyError:
                continue

            self.__locks[market_name] = dict()
            for asset_pair in os.listdir(MARKET_DATA_FOLDER+'/'+market_name):
                self.__locks[market_name][asset_pair] = dict()
                for timeframe in TIMEFRAMES.keys():
                    self.__locks[market_name][asset_pair][str(timeframe)] = threading.Lock()


        self.end_point_ohlcv = EndPointOHLCV(self.__locks)
        END_POINTS["ohlcv"] = self.end_point_ohlcv.get


    def do_response(self, end_point, args):
        end_point = end_point.replace('/','').lower()

        try:
            validate(end_point, args)
            return END_POINTS[end_point](args)

        except Exception as e:
            return json.dumps({
                "errors" : [e.msg],
                "notices" : [],
            })

    def start(self):
        if len(self.__recorders.keys()) == 0:
            print("Nothing to do.")
            return

        must_repair = check_datastore_timeframes()
        for args in must_repair:
            restore_timeframe(*args)

        for market_name in self.__recorders.keys():
            self.__recorders[market_name].start()

        super(HTTPServer,self).serve_forever()
    
    def stop(self):
        for market_name in self.__recorders.keys():
            self.__recorders[market_name].terminate()

        self.socket.close()

    def server_forever(self):
        self.start()

#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import os
import json

from dsociepy.finance.framework.helpers import get_market_data_filename
from dsociepy.finance.framework.helpers import get_timestamp_by_filename
from dsociepy.finance.framework.helpers import get_timestamp_by_timeframe
from dsociepy.finance.framework.helpers import MARKET_DATA_FOLDER
from dsociepy.finance.framework.server.exceptions import TimeframeNotAvailable
from dsociepy.finance.framework.server.exceptions import UnknownAssetPair
from dsociepy.finance.framework.server.exceptions import UnknownMarket
from dsociepy.finance.framework.server.exceptions import WrongWindow
from dsociepy.finance.framework.data.integrity import check_data_contiguity

def end_point_pairs(args):
    response = {
        "values": {}
    }
    for market in os.listdir(MARKET_DATA_FOLDER):
        response["values"][market] = {}
        for asset in os.listdir(MARKET_DATA_FOLDER+'/'+market):
            response["values"][market][asset] = sorted([int(t) for t in os.listdir(MARKET_DATA_FOLDER+'/'+market+'/'+asset)])

    return json.dumps(response)

class EndPointOHLCV:
    def __init__(self, locks):
        self.__locks = locks

    def get(self, args):
        response = {
            "values":[],
            "notices" : [],
            "errors" : []
        }
        # TESTING ARGS
        if not args["market"] in os.listdir(MARKET_DATA_FOLDER):
            raise UnknownMarket
    
        if not args["pair"] in os.listdir(MARKET_DATA_FOLDER+'/'+args["market"]):
            raise UnknownAssetPair

        if not args["timeframe"] in os.listdir(MARKET_DATA_FOLDER+'/'+args["market"]+'/'+args["pair"]):
            raise TimeframeNotAvailable

        if int(args["window"]) < 1:
            raise WrongWindow

        folder = MARKET_DATA_FOLDER+'/'+args["market"]+'/'+args["pair"]+'/'+args["timeframe"]+'/'
        folder_content = sorted(os.listdir(folder))

        # IF THERE IS NO DATA...
        try:
            self.__locks[args["market"]][args["pair"]][args["timeframe"]].acquire()
            latest_timestamp = json.load(open(folder+folder_content[-1], 'r'))[-1][0]
            oldest_timestamp = json.load(open(folder+folder_content[0], 'r'))[0][0]
            self.__locks[args["market"]][args["pair"]][args["timeframe"]].release()

        except IndexError:
            self.__locks[args["market"]][args["pair"]][args["timeframe"]].release()
            response["notices"] = ["REQUEST_LATEST_DATA", "REQUEST_OLDEST_DATA", "MISSING_DATA"]
            return json.dumps(response)

        # SETUP TIMESTAMP
        if int(args["timestamp"]) == -1:
            response["notices"].append("REQUEST_LATEST_DATA")
            timestamp = latest_timestamp
        
        elif int(args["timestamp"]) >= latest_timestamp:
            response["notices"].append("REQUEST_LATEST_DATA")
            timestamp = latest_timestamp

        else:
            timestamp = get_timestamp_by_timeframe(args["timestamp"], args["timeframe"])
    
        # DEFINE INVOLVED FILENAMES
        timestamp_offset = timestamp - ((int(args["window"])-1) * int(args["timeframe"]))
    
        # DETECT OLDEST DATA
        if timestamp_offset <= oldest_timestamp:
            response["notices"].append("REQUEST_OLDEST_DATA")
    
        for i in range(0, len(folder_content)):
            chunk_timestamp = get_timestamp_by_filename(folder_content[i], args["timeframe"])
            try:
                chunk_p_1_timestamp = get_timestamp_by_filename(folder_content[i+1], args["timeframe"])
                b1 = timestamp_offset < chunk_timestamp or (timestamp_offset >= chunk_timestamp and timestamp_offset < chunk_p_1_timestamp)
        
            except IndexError:
                b1 = True

            b2 = timestamp >= chunk_timestamp
        
            if b1 and b2:
	        # LOAD INVOLVED RECORDS
                try:
                    self.__locks[args["market"]][args["pair"]][args["timeframe"]].acquire()
                    data = json.load(open(folder+folder_content[i], 'r'))
                    self.__locks[args["market"]][args["pair"]][args["timeframe"]].release()
                    for frame in data:
                        if timestamp_offset <= frame[0] and frame[0] <= timestamp and len(frame) > 1:
                            response["values"].append(frame)


                except FileNotFoundError as e:
                    self.__locks[args["market"]][args["pair"]][args["timeframe"]].release()
                    pass
        
        if len(response["values"]) <  int(args["window"]) and not "MISSING_DATA" in response["notices"]:
            response["notices"].append("MISSING_DATA")
        
        return json.dumps(response)

def end_point_oldest_ohlcv_window(args):
        response = {
            "values":[],
            "notices" : [],
            "errors" : []
        }
        # TESTING ARGS
        if not args["market"] in os.listdir(MARKET_DATA_FOLDER):
            raise UnknownMarket
    
        if not args["pair"] in os.listdir(MARKET_DATA_FOLDER+'/'+args["market"]):
            raise UnknownAssetPair

        if int(args["window"]) < 1:
            raise WrongWindow

        folder = MARKET_DATA_FOLDER+args["market"]+'/'+args["pair"]+'/'
        folder_content = sorted([ int(v) for v in os.listdir(folder)])
        folder += str(folder_content[0])
        folder_content = sorted(os.listdir(folder))
        try:
            remain = int(args["window"])
            values = []
            for filename in folder_content:
                data = json.load(open(folder+'/'+filename, 'r'))
                if len(data) >= remain:
                    values += data[:remain]
                    break

                else:
                    remain -= len(data)
                    values += data

            response["values"] += values

        except Exception as e:
            response["errors"].append(str(e))
        
        return json.dumps(response)

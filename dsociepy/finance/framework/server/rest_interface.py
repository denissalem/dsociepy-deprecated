#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from http.server import BaseHTTPRequestHandler
from urllib.parse import urlparse
from urllib.parse import parse_qs

class RestInterface(BaseHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        self.__callback = server.do_response
        super(BaseHTTPRequestHandler, self).__init__(request, client_address, server)
    
    def _set_headers(self):
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        pass

    def do_GET(self):
        self._set_headers()
        end_point = urlparse(self.path).path
        args = {}
        p_qs = parse_qs(urlparse(self.path).query)
        for key in p_qs.keys():
            args[key] = p_qs[key][0]

        response = self.__callback(end_point, args)
        self.wfile.write(response.encode("utf8"))
        return
    
    def log_message(self, frmt, *args):
        pass


#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import time
from dsociepy.finance.framework.helpers import date_from_time_stamp

class NoEndPoint(Exception):
    msg = "No end point."

class UnknownEndPoint(Exception):
    msg = "Unknown end point."

class TooMuchArgs(Exception):
    msg = "Too much arguments."

class MissingOrWrongArgs(Exception):
    msg = "Missing or wrong arguments."

class WrongArgsType(Exception):
    msg = "One or more arguments has wrong type."

class UnknownMarket(Exception):
    msg = "This market isn't available on this server."

class UnknownAssetPair(Exception):
    msg = "This asset pair isn't available on this server."

class TimeframeNotAvailable(Exception):
    msg = "This timeframe isn't available for this market."

class WrongWindow(Exception):
    msg = "Window must be greater or equal than 1."

class ServerError(Exception):
    def __init__(self, msg):
        super(Exception, self).__init__(
            msg,
            date_from_time_stamp(time.time())            
        )

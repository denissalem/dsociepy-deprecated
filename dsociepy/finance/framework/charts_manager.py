#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from dsociepy.finance.framework.user_interfaces.charts_manager import ChartsManager as UserInterface
from dsociepy.finance.framework.user_interfaces.charts_by_timeframe import ChartsByTimeframe

from dsociepy.finance.framework.helpers import TAB_NAMES_BY_TIMEFRAMES

class ChartsManager:
    def __init__(self, parent):
        self.__parent = parent
        self.__charts = []
        self.user_interface = UserInterface(self.__charts, self.__add_chart)

    def __add_chart(self, widget):
        self.__parent.trade.user_interface.overview.set_current_page(0)
        for key in TAB_NAMES_BY_TIMEFRAMES.keys():
            if TAB_NAMES_BY_TIMEFRAMES[key] == widget.get_label():
                self.__charts.append(ChartsByTimeframe(self.__parent.trade, key))
                break

        self.__charts[-1].set_price_indicators(1000)
        self.__charts[-1].close.connect("clicked", self.__close_chart, self.__charts[-1])
        page, tab = self.__charts[-1].get_ui()
        self.__parent.user_interface.charts.append_page(
            page,
            tab
        )
        self.__parent.user_interface.charts.set_current_page(self.__parent.user_interface.charts.get_n_pages()-1)
        self.__parent.user_interface.charts.set_tab_reorderable(self.__charts[-1].page, True)
        self.__parent.user_interface.charts.set_tab_detachable(self.__charts[-1].page, True)
        self.__parent.user_interface.charts.connect("switch-page", self.__switch_page)
        self.__parent.user_interface.charts.show_all()
        self.start_all_widgets()

    def get_backends(self):
        return self.__charts

    def __close_chart(self, widget, chart):
        self.__parent.user_interface.remove_page(chart.page)
        self.__charts.remove(chart)
        self.__parent.trade.global_data_handler.remove(chart.get_data_handler())
        chart.stop_all_widgets()

    def __switch_page(self, notebook, page, page_num):
        for chart in self.__charts:
            index = self.__parent.user_interface.charts.page_num(chart.page)
            if -1 == index:
                chart.start_all_widgets()
            
            else:
                if index == page_num:
                    chart.start_all_widgets()
                
                else:
                    chart.stop_all_widgets()

    def stop_all_widgets(self):
        for chart in self.__charts:
            p, t = chart.get_ui()
            page_num = self.__parent.user_interface.charts.page_num(p)
            if page_num != -1:
                chart.stop_all_widgets()
    
    def start_all_widgets(self):
        for chart in self.__charts:
            p, t = chart.get_ui()
            page_num = self.__parent.user_interface.charts.page_num(p)
            if page_num == self.__parent.user_interface.charts.get_current_page():
                chart.start_all_widgets()
            
            elif page_num != -1:
                chart.stop_all_widgets()

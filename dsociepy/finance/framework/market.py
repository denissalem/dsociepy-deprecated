#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import threading
from dsociepy.finance.framework.setup_market import SetupMarket
from dsociepy.finance.framework.setup_trade import SetupTrade
from dsociepy.finance.framework.trade import Trade
from dsociepy.finance.framework.conductor import Conductor
from dsociepy.finance.framework.setup_market import market_interface_factory
from dsociepy.finance.framework.user_interfaces.are_you_sure import AreYouSure
from dsociepy.finance.framework.user_interfaces.market import Market as UserInterface

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib

import os

class Market:
    def __init__(self, name, parent_context):
        if not name in os.listdir(parent_context.data_folder+'/MarketData'):
            os.mkdir(parent_context.data_folder+'/MarketData/'+name)

        self.trades = list()
        self.terminated = False
        self.conductor_running = True
        self.name = name
        self.parent_context = parent_context
        
        # MARKET STATES
        self.states = parent_context.states
        if not name in self.states["markets"].keys():
            self.states["markets"][name] = {"setup":{},"assetPairs":{},"trades":{}}
            self.states.save_as_json()

        self.restore_market_interface()

        # USER INTERFACE
        self.user_interface = UserInterface(self)
        self.user_interface.quit.connect("clicked", self.quit)
        self.user_interface.setup.connect("clicked", self.setup_market)
        self.user_interface.trade.connect("clicked", self.trade)
        self.user_interface.run_trades.connect("clicked", self.run_trades)
        self.user_interface.pause_trades.connect("clicked", self.pause_trades)

        # RESTORE TRADES
        for trade_id in self.states["markets"][name]["trades"].keys():
            self.append_trade(
                self.states["markets"][name]["trades"][trade_id]["assetPair"],
                trade_id,
                self.states["markets"][name]["trades"][trade_id]["backtest"]
            )
        
        # RUN THREADS
        self.conductor = Conductor(self)
        self.conductor.start()
        GLib.timeout_add(500, self.__conductor_handler)
    
    def __conductor_handler(self):
        if not self.conductor.isAlive() and self.name in self.states["markets"]:
            timer_real, index = self.conductor.get_states()
            self.conductor = Conductor(self, timer_real, index)
            self.conductor.start()
            
        return self.conductor_running

    def restore_market_interface(self):
        self.market_interface = market_interface_factory(self.name, self.states["markets"][self.name]["setup"])

    def run_trades(self, widget=None):
        for trade in self.trades:
            if trade.user_interface.control.get_label() == "Run":
                trade.running = True
                trade.user_interface.control.set_label("Pause")

    def pause_trades(self, widget=None):
        for trade in self.trades:
            if trade.user_interface.control.get_label() == "Pause":
                trade.running = False
                trade.user_interface.control.set_label("Run")

    def quit(self, widget):
        self.quit_prompt = AreYouSure(self.parent_context.user_interface, "Are you sure you want to quit this market?")        

        if self.quit_prompt.response == -8:
            self.conductor_running = False
            self.conductor.join()
            
            self.parent_context.user_interface.markets.remove_page(
                self.parent_context.user_interface.markets.page_num(self.user_interface.page)
            )

            self.states["markets"].pop(self.name)
            self.states.save_as_json()
            self.terminated = True
    
    def setup_market(self, widget):
        self.setup_market = SetupMarket(self, self.name)
    
    def trade(self, widget):
        self.setup_trade = SetupTrade(self)

    def get_ui(self):
        return (
            self.user_interface.page,
            self.user_interface.tab
        )

    def get_current_trade_page(self):
        return self.user_interface.trades.get_current_page()

    def append_trade(self, asset_pair, identifier, backtest):
        trade = Trade(
            self,
            asset_pair,
            identifier,
            backtest
        )

        for indicator in self.parent_context.indicators:
            if indicator[0] == "lagging":
                trade.set_lagging_indicators(indicator[1])

        self.trades.append(trade)
        page, tab = self.trades[-1].get_ui()
        self.user_interface.append_trade(page, tab)

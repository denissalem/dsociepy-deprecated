#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

class IndicatorControl:
    def __init__(self, parent, user_interface_class, label):
        self.display = False
        self._parent = parent
        self._user_interface = user_interface_class(self, label)
        self._widget = None
    
    def add(self, widget_class):
        self._widget = widget_class(self._parent.get_data_handler())
        self._widget.set_sync_lock(self._parent.sync_lock)
        self._parent.panes_manager.add(self._widget)
        self._parent.indicators.append(self._widget)
    
    def remove(self):
        self._parent.indicators.remove(self._widget)
        self._parent.panes_manager.remove(self._widget)
        self._widget.destroy()

    def get_menu_item(self):
        return self._user_interface

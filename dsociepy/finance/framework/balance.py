#! /usr/bin/python3

from dsociepy.finance.framework.user_interfaces.balance import Balance as UserInterface

class Balance:
    def __init__(self):
        self.user_interface = UserInterface()
    
    def get_ui(self):
        return (self.user_interface.page, self.user_interface.name)
    

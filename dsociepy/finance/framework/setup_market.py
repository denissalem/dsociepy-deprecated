#! /usr/bin/python3

from dsociepy.finance.framework.user_interfaces.error import Error
from dsociepy.finance.framework.user_interfaces.setup_market import SetupMarket as UserInterface
from dsociepy.finance.framework.user_interfaces import Field
from dsociepy.finance.framework.market_interfaces.kraken import Kraken
from dsociepy.finance.framework.market_interfaces.poloniex import Poloniex
from dsociepy.finance.framework.market_interfaces.exceptions import APIErrors


NotNul = lambda x : True if x > 0 else False
NotEmpty = lambda x : True if len(x) > 0 else False
Integer = lambda x : True if x >= 0 else False

market_setup_fields = { 
    "Kraken" : [("Period",int, NotNul), ("API Key",str, NotEmpty), ("Private Key",str, NotEmpty)],
    "POLONIEX" : [("Period",int, NotNul), ("API Key",str, NotEmpty), ("Private Key",str, NotEmpty)]
}

market_interfaces = {
    "Kraken" : Kraken,
    "POLONIEX" : Poloniex,
    "IG Group" : Kraken
}

def market_interface_factory(name, setup):
    return market_interfaces[name](setup)

class SetupMarket:
    def __init__(self, market_handler, name):
        self.market_handler = market_handler
        self.states = market_handler.states
        self.name = name

        # Fields
        self.fields = []
        for item in market_setup_fields[self.name]:
            try:
                value = self.states["markets"][name]["setup"][item[0]]
            except KeyError:
                value = ''

            self.fields.append(
                Field(item[0], value)
            )
        
        # USER INTERFACE
        self.user_interface = UserInterface(self.fields)
        self.user_interface.button.connect("clicked", lambda widget : self.setup_market())

    def setup_market(self):
        setup = dict()
        try:
            current_field_name=str()
            for index in range(0, len(market_setup_fields[self.name])):
                current_field_name = market_setup_fields[self.name][index][0]
                value = market_setup_fields[self.name][index][1](self.fields[index].get_value())
                if not market_setup_fields[self.name][index][2](value):
                    raise ValueError

                setup[self.fields[index].name] = value
        
        except ValueError:
            self.error = Error([
                current_field_name+':',
                "invalid value"
            ])
            return

        market_interface = market_interface_factory(self.name, setup)
        
        try:
            market_interface.test_keys()
        
        except ValueError as e:
            self.error = Error([
                e.args[0]
            ])
            return
        
        except APIErrors as e:
            self.market_handler.user_interface.trade.set_sensitive(False)
            self.error = Error(e.errors)
            try:
                self.states["markets"][self.name]["setup"] = {}
                self.states["markets"][self.name]["assetPairs"] = {}
                self.states.save_as_json()

            except KeyError:
                pass

            self.states.save_as_json()
            return

        self.market_handler.restore_market_interface()
        self.user_interface.destroy()            

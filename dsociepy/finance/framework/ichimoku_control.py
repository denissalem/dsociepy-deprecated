#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from dsociepy.finance.framework.user_interfaces.indicator_control import IndicatorControl as UserInterface
from dsociepy.finance.framework.indicator_control import IndicatorControl

class IchimokuControl(IndicatorControl):
    def __init__(self, parent):
        super(IchimokuControl, self).__init__(parent, UserInterface, "Ichimoku Kinko Hyo")
    
    def add(self):
        try:
            self._parent._price_indicators.bind_ichimoku()

        except AttributeError:
            pass

    def remove(self):
        try:
            self._parent._price_indicators.unbind_ichimoku()

        except AttributeError:
            pass

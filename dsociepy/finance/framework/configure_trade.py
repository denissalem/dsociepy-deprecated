#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

import io
import importlib

import json
import pycurl
import threading

from dsociepy.finance.framework.user_interfaces.configure_trade import ConfigureTrade as UserInterface
from dsociepy.finance.framework.user_interfaces import Field
from dsociepy.finance.framework.data.mds_context import MarketDataServerContext

from dsociepy.finance.framework.user_interfaces.pick_mds import PickMDS
from dsociepy.finance.framework.reset_from import ResetFrom
from dsociepy.finance.framework.user_interfaces.load_strategy import LoadStrategy

class ConfigureTrade:
    def __init__(self, parent):
        self.market_name = parent.market_handler.name
        self.asset_pair = parent.asset_pair
        self.__parent = parent

        self.__servers = dict()

        self.__user_interface = UserInterface()

        self.pick_mds = PickMDS(
            self.__servers.keys(),
            self.__parent.states["trades"][self.__parent.id]["marketDataServer"]
        )
        self.__user_interface.pack_left(self.pick_mds)
        try:
            self.reset_from = ResetFrom(self.__parent.get_mds_context, self.__parent.states["trades"][self.__parent.id]["resetFrom"])

        except KeyError:
            self.reset_from = ResetFrom(self.__parent.get_mds_context, 0)

        self.__user_interface.pack_center_left(self.reset_from.get_ui())
        if self.__parent.is_backtest():
            self.reset_from.set_sensitive(True)

        else:
            self.reset_from.set_sensitive(False)

        self.__user_interface.pack_center_right(Gtk.Label("Nothing there."))

        try:
            strategy_package_name = self.__parent.states["trades"][self.__parent.id]["strategy"]
        except KeyError:
            strategy_package_name = ""

        self.load_strategy = Field("Strategy package name:", strategy_package_name)
        self.__user_interface.pack_right(self.load_strategy.frame)
        self.__user_interface.save_config_button.connect("clicked", self.__save)
        self.__thread = threading.Thread()
    
    def get_values(self):
        return {
            "marketDataServer" : self.pick_mds.get_values(),
            "strategy" : self.load_strategy.get_value(),
            "resetFrom" : self.reset_from.get_value()
        }

    def setup_tab(self):
        GLib.idle_add(self.__thread_warming_up)
        GLib.idle_add(self.reset_from.thread_warming_up)
    
    def __thread_warming_up(self):
        self.pick_mds.warming_up()
        if not self.__thread.isAlive():
            self.__thread = threading.Thread(target=self.__warming_up, daemon=True)
            self.__thread.start()

    def __warming_up(self):
        for server in self.__parent.get_mds_list():
            try:
                pairs = self.__get_pairs(server)
                if self.asset_pair in pairs[self.market_name].keys():
                    self.__servers[server[0]] = server[1]

                self.pick_mds.set()
            except KeyError:
                pass

            except Exception as e:
                raise e

        self.pick_mds.setup_servers(
            self.__servers.keys(),
            self.__parent.states["trades"][self.__parent.id]["marketDataServer"]
        )

    def get_ui(self):
        return (self.__user_interface.page, Gtk.Label("Configuration"))

    def __save(self, widget):
        values = self.get_values()

        if values["marketDataServer"] != '':
            self.__parent.mds_context = MarketDataServerContext(
                values["marketDataServer"],
                self.__servers[values["marketDataServer"]],
                self.__parent.market_handler.name,
                self.__parent.asset_pair
            )
        
        if values["strategy"] != '':
            try:
                strategy_package = getattr(__import__(values["strategy"], globals(), locals(), ["strategy"]), "strategy")
                if not type(self.__parent.strategy) == strategy_package.Strategy:
                    self.__parent.reload_strategy(strategy_package.Strategy)
                    
            except ModuleNotFoundError:
                print("Module not found")
        
        else:
            from dsociepy.finance.framework.strategy import BaseStrategy
            self.__parent.reload_strategy(BaseStrategy)

        for key in values:
            self.__parent.states["trades"][self.__parent.id][key] = values[key]

        self.__parent.states.save_as_json()

        
    def __get_pairs(self, server_info):
        server = server_info[0]
        port = server_info[1]
        try:
            buffer = io.BytesIO()
            query = pycurl.Curl()
            query.setopt(query.URL, server+"/pairs")
            query.setopt(query.PORT, port)
            query.setopt(query.WRITEFUNCTION, buffer.write)
            query.setopt(pycurl.CONNECTTIMEOUT, 5)
            query.perform()
            response = buffer.getvalue().decode('UTF-8')
            buffer.close()
            return json.loads(response)["values"]

        except pycurl.error as e:
            print(e)
            return {}

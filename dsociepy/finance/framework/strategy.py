#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from dsociepy.finance.framework.user_interfaces.strategy import BaseStrategy as UserInterface

class BaseStrategy:
    def __init__(self, set_running, global_data_handlers, default_ui=False, name="DEFAULT STRATEGY"):
        self.user_interface = None
        if default_ui:
            self.add_default_ui()

        self._name = name
        self.set_running = set_running

    def perform(self):
        print("Not implemented.")

    def __connect_default_widget(self):
        self.user_interface.get_reset_button().connect('clicked', self.__reset)
        self.user_interface.get_start_button().connect('clicked', self.__start)
        self.user_interface.get_stop_button().connect('clicked', self.__stop)

    def __reset(self, widget):
        print("reset")
    
    def __start(self, widget):
        self.set_running(True)

    def __stop(self, widget):
        self.set_running(False)
    
    def get_strategy_name(self):
        return self._name

    def add_default_ui(self):
        self.add_ui()
        self.user_interface.set_default()
        self.__connect_default_widget()

    def add_ui(self, ui_class=UserInterface, orientation_vertical = True):
        if self.user_interface == None:
            self.user_interface = ui_class(orientation_vertical)
            self.__connect_default_widget()

        else:
            print("UI has already been loaded.")

    def get_ui(self):
        return self.user_interface.get_ui()

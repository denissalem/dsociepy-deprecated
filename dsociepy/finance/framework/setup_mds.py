#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from dsociepy.finance.framework.user_interfaces.setup_mds import SetupMDS as UserInterface

class SetupMDS:
    def __init__(self, states):
        self.states = states
        self.user_interface = UserInterface(
            self.states["marketDataServers"]
        )

        self.user_interface.setup_button.connect("clicked", self.setup)

    def setup(self, widget):
        self.states["marketDataServers"] = list(self.user_interface.servers_list)
        self.states.save_as_json()

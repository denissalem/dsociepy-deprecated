#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from dsociepy.finance.framework.user_interfaces.reset_from import ResetFrom as UserInterface
from dsociepy.finance.framework.helpers import query_oldest_ohlcv_window
from dsociepy.finance.framework.helpers import query_latest_ohlcv_window
import threading

class ResetFrom:
    def __init__(self, get_mds_context, default_timestamp):
        self.__get_mds_context = get_mds_context
        self.__user_interface = UserInterface(default_timestamp)
        self.__thread = threading.Thread()

    def get_value(self):
        return self.__user_interface.get_value()
    
    def get_ui(self):
        return self.__user_interface

    def thread_warming_up(self):
        self.__user_interface.warming_up()
        if not self.__thread.isAlive():
            pass
            self.__thread = threading.Thread(target=self.__warming_up, daemon=True)
            self.__thread.start()

    def __warming_up(self):
        try:
            response = query_oldest_ohlcv_window(self.__get_mds_context)
            oldest_timestamp = response["values"][0][0]
            response = query_latest_ohlcv_window(self.__get_mds_context)
            latest_timestamp = response["values"][-1][0]
            self.__user_interface.set_range(latest_timestamp, oldest_timestamp)
            self.__user_interface.set()

        except Exception as e:
            print(e)

    def set_sensitive(self, value):
        self.__user_interface.set_sensitive(value)

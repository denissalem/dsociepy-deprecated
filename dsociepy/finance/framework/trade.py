#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

import importlib
import os
import time
import threading

from dsociepy.finance.framework.configure_trade import ConfigureTrade
from dsociepy.finance.framework.user_interfaces.are_you_sure import AreYouSure
from dsociepy.finance.framework.user_interfaces.reset_from import ResetFrom
from dsociepy.finance.framework.user_interfaces.error import Error
from dsociepy.finance.framework.user_interfaces.trade import Trade as UserInterface
from dsociepy.finance.framework.user_interfaces.save_market_data import SaveMarketData
from dsociepy.finance.framework.balance import Balance
from dsociepy.finance.framework.charts import Charts
from dsociepy.finance.framework.log import Log
from dsociepy.finance.framework.strategy import BaseStrategy
from dsociepy.finance.framework.configure_trade import ConfigureTrade
from dsociepy.finance.framework.market_interfaces.dummy import Dummy
from dsociepy.finance.framework.data.mds_context import MarketDataServerContext
from dsociepy.finance.framework.data.global_data_handler import GlobalDataHandler

class Trade:
    def __init__(self, market_handler, asset_pair, trade_id, backtest):
        self.__backtest = backtest
        self.global_data_handler = GlobalDataHandler(self.get_mds_context, backtest)
        self.__setup_market_interface(market_handler)
        
        self.market_handler = market_handler
        self.states = market_handler.states
        self.mds_list = self.states["marketDataServers"]
        self.asset_pair = asset_pair
        self.id = str(trade_id)
        self.running = False
        self.states = self.states["markets"][self.market_handler.name]

        # USER INTERFACE
        self.__setup_mds_context()
        self.charts = Charts(self)
        self.user_interface = UserInterface(self)

        page, name = self.charts.get_ui()
        self.user_interface.overview.append_page(page, name)

        self.balance = Balance()
        page, name = self.balance.get_ui()
        self.user_interface.overview.append_page(page, name)

        self.log = Log()
        page, name = self.log.get_ui()
        self.user_interface.overview.append_page(page, name)
        
        try:
            strategy_package = getattr(__import__(self.states["trades"][self.id]["strategy"], globals(), locals(), ["strategy"]), "strategy")
            self.strategy = strategy_package.Strategy(self.set_running, self.global_data_handler)

        except Exception as e:
            self.strategy = BaseStrategy(self.set_running, self.global_data_handler, default_ui=True)
            print("can't load strategy", e)
        
        page, name = self.strategy.get_ui()
        self.__strategy_box = Gtk.Box()
        self.__strategy_box.pack_start(page, True, True, 0)
        self.user_interface.overview.append_page(self.__strategy_box, name)

        self.configure = ConfigureTrade(self)
        page, name = self.configure.get_ui()
        self.user_interface.overview.append_page(page, name)
        self.user_interface.switch_page_disabled = False

    def set_running(self, value):
        self.running = value

    def perform_strategy(self):
        self.strategy.perform()

    def reload_strategy(self, strategy_class):
        try:
            page, name = self.strategy.get_ui()
            self.__strategy_box.remove(page)
            page.destroy()
        except Exception as e:
            print(e)
            pass
        
        self.strategy = strategy_class(self.set_running, self.global_data_handler)
        try:
            page, name = self.strategy.get_ui()

        except AttributeError:
            self.strategy.add_default_ui()
            page, name = self.strategy.get_ui()

        page.show_all()
        self.__strategy_box.pack_start(page, True, True, 0)

    def get_mds_list(self):
        return self.market_handler.states["marketDataServers"]

    def __setup_mds_context(self):
        server = ''
        port = 6473

        for mds in self.mds_list:
            if self.states["trades"][self.id]["marketDataServer"] == mds[0]:
                server = mds[0]
                port = mds[1]
                break

        self.mds_context = MarketDataServerContext(
            server,
            port,
            self.market_handler.name,
            self.asset_pair
        )

    def __setup_market_interface(self, market_handler):
        if self.is_backtest():
            self.__market_interface = Dummy()

        else:
            self.__market_interface = market_handler.market_interface

    def get_mds_context(self):
        return self.mds_context

    def is_backtest(self):
        return self.__backtest

    def is_chart_visible(self):
        return not self.chart.user_interface == None

    def get_page_num(self):
        return self.market_handler.user_interface.trades.page_num(self.user_interface.page)

    def get_tab_name(self):
        return self.asset_pair

    def get_ui(self):
        return (self.user_interface.page, self.user_interface.tab)

    def setup(self, widget):
        pass
    
    def close(self, widget):
        self.__quit_prompt = AreYouSure(self.market_handler.parent_context.user_interface, "Are you sure you want to close this trading?")        
        if self.__quit_prompt.response == -8:
            self.charts.stop_all_widgets()
            self.market_handler.user_interface.trades.remove_page(
                self.market_handler.user_interface.trades.page_num(self.user_interface.page)
            )

            self.states["trades"].pop(self.id)
            self.states.save_as_json()
            index = self.market_handler.trades.index(self)
            
            # FIX CONDUCTOR INDEX
            conductor_timer, conductor_index = self.market_handler.conductor.get_states()
            if index < conductor_index and conductor_index > 0:
                self.market_handler.conductor.decrement_index()

            self.market_handler.trades.remove(self)

            """ STOP ALL WIDGET"""

    def update_log(self, exception):
        if type(exception) == AttributeError:
            raise exception

        GLib.idle_add(self.log.update, self.market_handler.name, exception)

    def __reset_backtest(self, widget):
        pass
        #self.__reset_from.reset.connect("clicked", self.__reset)

    def __reset(self, widget):
        new_date = datetime.datetime(
                year = self.__reset_from.year.get_value_as_int(),
                month = self.__reset_from.month.get_value_as_int(),
                day = self.__reset_from.day.get_value_as_int(),
                hour = self.__reset_from.hour.get_value_as_int(),
                minute = self.__reset_from.minute.get_value_as_int()
            )
        self.__data_provider.reset(new_date.timestamp())
        self.charts.reset()


#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from dsociepy.finance.framework.helpers import RESSOURCE_FOLDER
from dsociepy.finance.framework.helpers import TAB_NAMES_BY_TIMEFRAMES

from dsociepy.finance.framework.user_interfaces.charts import Charts as UserInterface
from dsociepy.finance.framework.charts_manager import ChartsManager
from dsociepy.finance.framework.user_interfaces.tabs_manager import TabsManager

class Charts:
    def __init__(self, trade):
        self.trade = trade
        self.get_mds_context = trade.get_mds_context
        self.__charts_manager = ChartsManager(self)
        self.user_interface = UserInterface(self.__charts_manager.user_interface)
        self.__tabs_manager = TabsManager(
            self.get_backends(),
            trade.market_handler.name+', '+trade.asset_pair
        ) 
        
        self.user_interface.charts.set_group_name(trade.market_handler.name+', '+trade.asset_pair)
        self.user_interface.charts.connect("create-window", self.__tabs_manager.detach_tab)
    
    def get_backends(self):
        return self.__charts_manager.get_backends()

    def get_ui(self):
        return self.user_interface.get_ui()
    
    def stop_all_widgets(self):
        self.__charts_manager.stop_all_widgets()

    def start_all_widgets(self):
        self.__charts_manager.start_all_widgets()

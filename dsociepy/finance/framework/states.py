#! /usr/bin/python3
import json

class States(dict):
    def __init__(self, dataInput, path=None, root=None):
        self.root = self if root == None else root
        self.path = path

        for key in dataInput:
            self[key] = dataInput[key]

    def __setitem__(self, key, value):
        if type(value) == dict:
            super(States, self).__setitem__(key, States(value, root=self.root))

        else:
            super(States, self).__setitem__(key, value)

    def save_as_json(self):
        if self.root != None and self.root.path != None:
            open(self.root.path, "w").write(
                json.dumps(self.root)
            )

#! /usr/bin/python3

import os
from threading import Lock

from dsociepy.finance.framework.user_interfaces.log import Log as UserInterface
from dsociepy.finance.framework.helpers import date_from_time_stamp
from dsociepy.finance.framework.helpers import MARKET_LOG_FOLDER
from gi.repository import GLib

class Log:
    def __init__(self, with_ui=True, output_log=""):
        self.__output_log = output_log
        os.makedirs(MARKET_LOG_FOLDER, exist_ok=True)
        self.__with_ui = with_ui
        self.__lock = Lock()
        if self.__with_ui:
            self.user_interface = UserInterface()
            self.buffer = self.user_interface.text_view.get_buffer()
        
        self.reset()

    def reset(self):
        self.log = list()
        if self.__with_ui:
            self.buffer.set_text(''.join(self.log))
        
    def update(self, market_name, exception):
        if type(exception) != str:
            err_msg = str(exception.args[1])+"\t"+market_name+": "+exception.args[0]

        else:
            err_msg = str(date_from_time_stamp(time.time()))+"\t"+market_name+": "+exception

        self.log.append(err_msg+"\n")
        print(err_msg)
            
        self.log = self.log[-256:]
        if self.__with_ui:
            self.buffer.set_text(''.join(self.log[::-1]))

        if self.__output_log != "":
            self.__lock.acquire()
            try:
                log_content = open(MARKET_LOG_FOLDER+'/'+exception.args[1].strftime(self.__output_log), 'r').read()

            except FileNotFoundError:
                log_content = ''

            log_content+=err_msg+'\n'
            open(MARKET_LOG_FOLDER+'/'+exception.args[1].strftime(self.__output_log), 'w').write(log_content)
            self.__lock.release()


    def get_ui(self):
        if self.__with_ui:
            return (self.user_interface.page, self.user_interface.name)

        else:
            raise AttributeError("This log instance doesn't have user interface")

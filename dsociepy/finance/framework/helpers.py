#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import json
import os
import io
import time
import pycurl
import urllib.parse

TIMEFRAMES = {
    60      : "%Y-%m-%d",   #1 minute
    180     : "%Y-%m-%d",   #3 minutes
    300     : "%Y-%m-%d",   #5 minutes
    900     : "%Y-%m-%d",   #15 minutes
    1200    : "%Y-%m-%d",   #20 minutes
    1800    : "%Y-%m-%d",   #30 minutes
    3600    : "%Y-%m",      #1 hour
    7200    : "%Y-%m",      #2 hours
    10800   : "%Y-%m",      #3 hours
    14400   : "%Y-%m",      #4 hours
    86400   : "%Y",         #1 day
    604800  : "%Y"          #1 week
}

BASE_TIMEFRAMES_PER_MARKET = {
    "Kraken" : 60
}

TAB_NAMES_BY_TIMEFRAMES = {
    60      : "1M",
    180     : "3M",
    300     : "5M",
    900     : "15M",
    1200    : "20M",
    1800    : "30M",
    3600    : "1H",
    7200    : "2H",
    10800   : "3H",
    14400   : "4H",
    86400   : "1D",
    604800  : "1W"
}

MARKET_DATA_FOLDER = os.path.expanduser('~')+'/'+".DSOCIEPY/Finance/MarketData/"
MARKET_LOG_FOLDER = os.path.expanduser('~')+'/'+".DSOCIEPY/Finance/Log/"
RESSOURCE_FOLDER = os.path.expanduser('~')+'/'+".local/share/dsociepy/ressources/finance/"

def box_visible(box, begin, end):
    for t in box:
        if t >= begin and t <= end:
            return True

    return False

def repair_data_set(data_set, timestamp_start, window_length, timeframe):
    values = []
    if len(data_set["values"]) == 0:
        if (not "REQUEST_OLDEST_DATA" in data_set["notices"]) and (not "REQUEST_LATEST_DATA" in data_set["notices"]):
            timestamp_end = timestamp_start - (window_length - 1) * timeframe
            generator = range(timestamp_end, timestamp_start + timeframe, timeframe)

        else:
            return data_set

    elif "REQUEST_OLDEST_DATA" in data_set["notices"]:
        if "REQUEST_LATEST_DATA" in data_set["notices"]:
            timestamp_start = int(data_set["values"][-1][0])
            
        timestamp_end = int(data_set["values"][0][0])
        generator = range(timestamp_end, timestamp_start + timeframe, timeframe)

    else:
        timestamp_end = int(data_set["values"][-1][0] - (window_length - 1) * timeframe)
        generator = range(timestamp_end, int(data_set["values"][-1][0])+timeframe, timeframe)

    for timestamp in generator:
        try:
            index = [d[0] for d in data_set["values"]].index(timestamp)
            values.append(data_set["values"][index])

        except:
            values.append([timestamp])
    data_set["values"] = values
    return data_set

def query_oldest_ohlcv_window(get_mds_context, window=150):
    params = urllib.parse.urlencode({
        "market" : get_mds_context().market,
        "pair" : get_mds_context().pair,
        "window" : window
    })
    buffer = io.BytesIO()
    query = pycurl.Curl()
    query.setopt(query.URL, get_mds_context().server_name+'/oldest_ohlcv_window?'+params)
    query.setopt(query.PORT, get_mds_context().port)
    query.setopt(query.WRITEFUNCTION, buffer.write)
    query.perform()
    response = buffer.getvalue().decode('UTF-8')
    buffer.close()
    return json.loads(response)

def query_latest_ohlcv_window(get_mds_context, window=150):
    params = urllib.parse.urlencode({
        "market" : get_mds_context().market,
        "pair" : get_mds_context().pair,
        "window" : window,
        "timeframe": BASE_TIMEFRAMES_PER_MARKET[get_mds_context().market],
        "timestamp":-1
    })
    buffer = io.BytesIO()
    query = pycurl.Curl()
    query.setopt(query.URL, get_mds_context().server_name+'/ohlcv?'+params)
    query.setopt(query.PORT, get_mds_context().port)
    query.setopt(query.WRITEFUNCTION, buffer.write)
    query.perform()
    response = buffer.getvalue().decode('UTF-8')
    buffer.close()
    dataset = json.loads(response)
    if "MISSING_DATA" in dataset["notices"]:
        dataset = self._repair_data_set(dataset, dataset["values"][-1][0], self._window_length)


def lookup_timeframe_dependancy(input_timeframe):
    timeframes = sorted(TIMEFRAMES.keys())[::-1]
    for timeframe in timeframes:
        if timeframe < input_timeframe:
            f = input_timeframe / timeframe
            i = int(f)
            if i == f:
                return timeframe

def get_timestamp_by_filename(filename, timeframe):
    return datetime.datetime.strptime(
	filename.replace(".json",''),
	TIMEFRAMES[int(timeframe)]
    ).timestamp()

def synthetize_ohlc(base_folder, asset_pair, timeframe, timestamp):
    lower_timeframe = lookup_timeframe_dependancy(timeframe)
    data_source_folder = base_folder+'/'+asset_pair+'/'+str(lower_timeframe)+'/'
    filename = get_market_data_filename(timestamp, lower_timeframe)
    data_source = list()
    try:
        lower_timeframe_data = json.load(open(data_source_folder+filename, 'r'))
    
    except FileNotFoundError:
        return []

    for ohlc in lower_timeframe_data:
        new_timestamp = get_timestamp_by_timeframe(ohlc[0], timeframe)
        try:
            index = [d[0] for d in data_source].index(new_timestamp)
        except ValueError:
            if len(ohlc[1:]):
                data_source.append([new_timestamp]+ohlc[1:])
            continue
        
        if len(ohlc) > 1:
            if ohlc[2] > data_source[index][2]:
                data_source[index][2] = ohlc[2]

            if ohlc[3] < data_source[index][3]:
                data_source[index][3] = ohlc[3]

            data_source[index][4] = ohlc[4]
            data_source[index][5] += ohlc[5]
            
    return data_source

def write_ohlc_per_filenames(destination_folder, filenames):
    os.makedirs(destination_folder, exist_ok=True)
    for filename in filenames.keys():
        try:
            saved_period_data = json.load(open(destination_folder+filename, 'r'))

        except FileNotFoundError:
            saved_period_data = []
            open(destination_folder+filename, "w").write(
                json.dumps(saved_period_data)
            )

        try:
            # copying...
            for ohlc in filenames[filename]:
                try:
                    index = [ d[0] for d in saved_period_data ].index(ohlc[0])
                    if len(ohlc) > 1:
                        saved_period_data[index] = ohlc

                except ValueError as e:
                    saved_period_data.append(ohlc)

            open(destination_folder+'/'+filename, "w").write(
                json.dumps(
                    sorted(saved_period_data, key=lambda d : d[0])
                )
            )

        except IndexError as e: 
            # when saved_period_data is []
            saved_period_data = filenames[filename]
            open(destination_folder+'/'+filename, "w").write(
                json.dumps(period_data)
            )

def set_ohlc_per_filenames(data_source, timeframe):
    filenames = {}
    for d in data_source:
        ohlc = [float(v) for v in d]
        filename = get_market_data_filename(ohlc[0], timeframe)
        try:
            filenames[filename].append(ohlc)

        except KeyError:
            filenames[filename] = [ ohlc ]

    return filenames

def get_timestamp_by_timeframe(timestamp, timeframe):
    return (int(timestamp) // int(timeframe)) * int(timeframe)

def date_from_time_stamp(timestamp):
    return datetime.datetime.fromtimestamp(timestamp)

def date_formater(previous, current):
        p = date_from_time_stamp(previous)
        c = date_from_time_stamp(current)

        if previous == 0:
            return c.strftime("%d %b %Y, %H:%M")

        else:
            for f in ["%Y", "%d %b", "%H:%M"]:
                if c.strftime(f) != p.strftime(f):
                    return (c.strftime(f), f)
                
        return "error"

def get_market_data_filename(timestamp, timeframe):
    return datetime.datetime.fromtimestamp(int(timestamp)).strftime(TIMEFRAMES[int(timeframe)])+".json"

def get_horizon_index(dataSet, limit):
    indexRange = 0
    for t in dataSet:
        if t < limit:
            indexRange +=1

        else:
            return indexRange

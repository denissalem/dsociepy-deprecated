#! /usr/bin/python3

#    Copyright 2016, 2017 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import base64
import hashlib
import hmac
import io
import json
import pycurl
import time
import urllib
import urllib.parse

from dsociepy.finance.framework.market_interfaces.exceptions import ResponseIsNotJSON
from dsociepy.finance.framework.market_interfaces.exceptions import APINotAvailable
from dsociepy.finance.framework.market_interfaces.exceptions import APIErrors

class Poloniex:
    def __init__(self, setup):
        self.setup = setup
        self.simulated_market_data = None

    def __setup_query(self, query, nonce, post_fields, uri_path):
        pass

    def public_query(self, method, data):
        pass

    def private_query(self, method, data):
        pass

    def get_orders_status(self, txids):
        pass

    # testing conectivity
    def get_trade_balance(self, asset="ZUSD"):
        pass

    def get_asset_pairs(self):
        pass

    def get_ohlc(self, pair, interval, since):
        pass

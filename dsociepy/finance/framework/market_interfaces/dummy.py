#! /usr/bin/python3

import json

from dsociepy.finance.framework.helpers import date_from_time_stamp
from dsociepy.finance.framework.market_interfaces.exceptions import EndOfSimulation

class Dummy:
    def __init__(self):
        self.reset_from()

    def get_oldest(self):
        pass

    def get_latest(self):
        pass

    def reset_from(self, timestamp=None):
        pass

    def get_ohlcv(self, pair, since):
        pass

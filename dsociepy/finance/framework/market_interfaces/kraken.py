#! /usr/bin/python3

#    Copyright 2016, 2017 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import base64
import hashlib
import hmac
import io
import json
import pycurl
import time
import urllib
import urllib.parse

from dsociepy.finance.framework.market_interfaces.exceptions import ResponseIsNotJSON
from dsociepy.finance.framework.market_interfaces.exceptions import APINotAvailable
from dsociepy.finance.framework.market_interfaces.exceptions import APIErrors

BASE_TIMEFRAME = 60

class Kraken:
    def __init__(self, setup=None):
        self.base_timeframe = BASE_TIMEFRAME
        self.simulated_market_data = None
        try:
            self.__set_keys(setup["API Key"], setup["Private Key"])
        except:
            pass

    def __set_keys(self, api_key, private_key):
        self.__api_key = api_key
        self.__private_key = private_key
    
    def __setup_private_query(self, query, nonce, post_fields, uri_path):
        try:
            # Taken from krakenex
            encoded = (nonce + post_fields).encode()
            message = uri_path.encode() + hashlib.sha256(encoded).digest()
            signature = hmac.new(
                base64.b64decode(self.__private_key),
                message,
                hashlib.sha512
            )

            sigdigest = base64.b64encode(signature.digest())
            query.setopt(
                query.HTTPHEADER, 
                [
                    "API-Key: "+self.__api_key,
                    "API-Sign: "+sigdigest.decode()
                ]
            )
            return query

        except AttributeError:
            raise AttributeError("Private Key and/or API Key not set.")

    def public_query(self, method, data):
        try:
            data["nonce"] = str(int(time.time() * 1000))
            uri_path = "/0/public/"+method
            post_fields = urllib.parse.urlencode(data)
            buffer = io.BytesIO()
            query = pycurl.Curl()
            query.setopt(query.URL, "https://api.kraken.com"+uri_path)
            query.setopt(query.WRITEFUNCTION, buffer.write)
            query.setopt(query.POSTFIELDS, post_fields)
            query.perform()
            response = buffer.getvalue().decode('UTF-8')
            buffer.close()

        except Exception as e:
            raise APINotAvailable(e)

        try:
            return json.loads(response)
    
        except:
            raise ResponseIsNotJSON(response) 

    def private_query(self, method, data):
        if self.__private_key == None or self.__api_key == None:
            raise ValueError("Private/API are keys are missing.")

        try:
            data["nonce"] = str(int(time.time() * 1000))
            post_fields = urllib.parse.urlencode(data)
            buffer = io.BytesIO()
            query = pycurl.Curl()
            uri_path = "/0/private/"+method
            query.setopt(query.URL, "https://api.kraken.com"+uri_path)
            query.setopt(query.WRITEFUNCTION, buffer.write)
            query.setopt(query.POSTFIELDS,post_fields)

            query = self.__setup_private_query(
                query,
                data["nonce"],
                post_fields,
                uri_path
            )

            query.perform()
            response = buffer.getvalue().decode('UTF-8')
            buffer.close()

        except Exception as e:
            raise e
            raise APINotAvailable(e)

        try:
            return json.loads(response)

        except Exception as e:
            raise ResponseIsNotJSON(response)

    def get_orders_status(self, txids):
        states_dict = {}
        # We split identifiers list into set of 20 chunks (this is the maximumm authorized by API).
        order_ids_sublists = [txids[x:x+20] for x in range(0, len(txids), 20)]
        for order_ids_sublist in order_ids_sublists:
            order_info_dict = self.private_query("QueryOrders",{"trades":"false","txid":",".join(order_ids_sublist)})["result"]
            for order_id in order_info_dict.keys():
                states_dict[order_id] = order_info_dict[order_id]["status"]

        return states_dict

    def test_keys(self):
        response = self.private_query("Balance", {})
        if len(response["error"]) != 0:
            raise ValueError(response["error"])

    def get_asset_pairs(self):
        response = self.public_query("AssetPairs", {})
        output = list()
        if len(response["error"]) != 0:
            raise APIErrors(["GetAssetPairs"]+response["error"])

        for asset in response["result"].keys():
            if asset[-2:] != ".d":
                output.append(asset)
        
        return output

    def get_ohlcv(self, pair, since):
        data = {"pair":pair,"interval": int(self.base_timeframe / 60), "since":since} 
        response = self.public_query("OHLC", data)
        
        if len(response["error"]) != 0:
            raise APIErrors(response["error"])
        
        #       <time>, <open>, <high>, <low>, <close>, <volume>
        return [ [d[0], d[1],   d[2],   d[3],   d[4],   d[6]] for d in response["result"][pair]]

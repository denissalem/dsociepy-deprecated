#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import time
from dsociepy.finance.framework.helpers import date_from_time_stamp

class ResponseIsNotJSON(Exception):
    def __init__(self, response):
        super(Exception, self).__init__(
            "Response is not JSON",
            date_from_time_stamp(time.time()),
            response
        )

class APINotAvailable(Exception):
    def __init__(self, exception=None):
        super(Exception, self).__init__(
            "API not available.",
            date_from_time_stamp(time.time()),
            exception
        )

class APIErrors(Exception):
    def __init__(self, errors):
        super(Exception, self).__init__(
            '\n'.join(errors),
            date_from_time_stamp(time.time()),
            errors
        )

class EndOfSimulation(Exception):
    def __init__(self):
        super(Exception, self).__init__(
            "End of simulation.",
            date_from_time_stamp(time.time())
        )

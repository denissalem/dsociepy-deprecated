#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib

import json
import os

from dsociepy.finance.framework.states import States 
from dsociepy.finance.framework.user_interfaces.are_you_sure import AreYouSure
from dsociepy.finance.framework.user_interfaces import MainWindow as UserInterface
from dsociepy.finance.framework.user_interfaces.join_market import JoinMarket 
from dsociepy.finance.framework.market import Market
from dsociepy.finance.framework.setup_market import market_interface_factory
from dsociepy.finance.framework.setup_mds import SetupMDS as SetupMDS

home_folder = os.path.expanduser('~')+'/'

class FinanceFramework:
    def __init__(self):
        self.indicators = list()
        self.markets = list()
        self.data_folder = home_folder+'/'+".DSOCIEPY/Finance/"
        os.makedirs(self.data_folder, exist_ok=True)
        # STATES

        try:
            with open(self.data_folder+"states.json") as raw:
                data = json.load(raw)
                self.states = States(data, path=self.data_folder+"states.json")

        except FileNotFoundError:
            if not 'states.json' in os.listdir(self.data_folder):
                self.__default_states()

        except ValueError:
            self.__default_states()

        if not "MarketData" in os.listdir(self.data_folder):
            os.mkdir(self.data_folder+"MarketData")

        # USER INTERFACE
        self.user_interface = UserInterface(self)
        self.user_interface.join_market.connect("clicked", self.__join_market)
        self.user_interface.run_markets.connect("clicked", self.__run_markets)
        self.user_interface.pause_markets.connect("clicked", self.__pause_markets)
        self.user_interface.setup_mds.connect("clicked", self.__setup_mds)

        self.__restore_markets()

    def run(self):
        if 0 < len(self.markets) and 0 < len(self.markets[0].trades):
            # Turn on first visible widgets
            overview_current_page = self.markets[0].trades[0].user_interface.overview.get_current_page()
            """ NEED TO BE MODIFIED """
            #if overview_current_page == 0:
            #    self.markets[0].trades[0].charts.start_all_widgets()
        
        # RUN USER INTERFACE
        self.user_interface.show_all()
        self.user_interface.connect("delete-event", self.__destroy) 
        Gtk.main()
        
    def __setup_mds(self, widget):
        self.__setup_mds = SetupMDS(self.states)

    def __default_states(self):
        open(self.data_folder+"/states.json","w").write(
            json.dumps({
                "marketDataServers": [],
                "markets": {}
            })
        )
        self.states = States({"marketDataServers": [],"markets":{}}, path=self.data_folder+"states.json")
        
    
    def __restore_markets(self):
        self.markets = list()
        for name in self.states["markets"].keys():
            self.append_market(name)

    def append_market(self, name):
        self.markets.append(Market(name, self))
        page, tab = self.markets[-1].get_ui()
        self.user_interface.markets.append_page(
            page,
            tab
        )
        page.show_all()

    def __run_markets(self, widget):
        for market in self.markets:
            market.run_trades()
    
    def __pause_markets(self, widget):
        for market in self.markets:
            market.pause_trades()

    def __join_market(self, widget):       
        self.__join_market_ui = JoinMarket(self)

    def __destroy(self, widget, event):
        self.__quit_prompt = AreYouSure(self.user_interface, "Are you sure you want to quit?")
        if self.__quit_prompt.response == -8:
            for market in self.markets:
                print("Exit",market.name+"...")
                for trade in market.trades:
                    trade.charts.stop_all_widgets()

                market.conductor_running = False
                market.conductor.join()
            
            Gtk.main_quit(widget, event)

        return True

#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import cairo

def draw_ohlc(chart, cairo_ctx):
    """Callback for drawing candlesticks.
        
    cairo_ctx : cairo.context -- ."""
    cairo_ctx.set_antialias(cairo.ANTIALIAS_NONE)

    x_origin = chart.get_x_origin()
    window = chart.get_window()
    candlestick_width = x_origin / (window - 1)
        
    data_set = chart.get_dataset("OHLC").yield_data()

    for candlestick in data_set:
        try:
            i = 1 + chart.get_time_range_index(candlestick[0])

        except Exception as e:
            continue

        if len(candlestick) > 1 :
            if candlestick[1] < candlestick[4]:
                cairo_ctx.set_source_rgb(0, 0.75, 0)
            
            else:
                cairo_ctx.set_source_rgb(0.75, 0, 0)

            top = candlestick[1] if candlestick[1] > candlestick[4] else candlestick[4]
            top = chart.compute_y_coordinate(top)
            
            bottom = candlestick[4] if candlestick[1] > candlestick[4] else candlestick[1]
            bottom = chart.compute_y_coordinate(bottom) - top

            if bottom < 1:
                cairo_ctx.move_to(x_origin - i * candlestick_width, top)
                cairo_ctx.line_to(x_origin - (i-1) * candlestick_width - 2, top) 
                cairo_ctx.stroke()

            else:
                cairo_ctx.rectangle(
                    x_origin - i * candlestick_width,
                    top,
                    candlestick_width-2,
                    bottom
                )
                cairo_ctx.fill()
            
            x = x_origin - i * candlestick_width + (candlestick_width-2)/2

            start = chart.compute_y_coordinate(candlestick[2])
            end = chart.compute_y_coordinate(candlestick[3])
            cairo_ctx.set_line_width(1)
            cairo_ctx.move_to(x, start)
            cairo_ctx.line_to(x, end) 
            cairo_ctx.stroke()
            
        else:
            cairo_ctx.set_source_rgb(0, 0, 0)
            cairo_ctx.rectangle(
                x_origin - i * candlestick_width,
                chart.compute_y_coordinate(0),
                int(candlestick_width)+1,
                chart.compute_y_coordinate(-1) - chart.compute_y_coordinate(0)
            )
            cairo_ctx.fill()
            
        i+=1
        if x_origin - i * candlestick_width < -candlestick_width:
            break

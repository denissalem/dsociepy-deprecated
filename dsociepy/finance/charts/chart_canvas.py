#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import gi
import cairo
import time
import threading
import numpy as np

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf, GLib

from dsociepy.finance.framework.helpers import date_formater
from dsociepy.finance.framework.helpers import date_from_time_stamp
from dsociepy.finance.framework.server.exceptions import ServerError
from dsociepy.finance.framework.market_interfaces.exceptions import APINotAvailable
from dsociepy.finance.framework.market_interfaces.exceptions import ResponseIsNotJSON

MARGIN_TOP = 10
MARGIN_BOTTOM = 25
MARGIN_RIGHT = 50

class ChartCanvas(Gtk.DrawingArea):
    def __init__(self, data_handler):
        self._master = time.time()
        self._sync = None
        self._data_handler = data_handler
        self._timeframe = self._data_handler.get_timeframe()
        self._indicator_key = ""
        self.max_candlesticks = 500
        self.min_candlesticks = 25
        self.__has_acquired_data = False
        self._local_lock = threading.Lock()
        self._draw_ruler_x_lock = threading.Lock()
        self._draw_ruler_x = False
        self._title = ""
        self._subtitle = ""
        self._window = 150
        self._run = False
        self._timeout = 1000
        self._write_buffering_params = True
        self._buffer = None
        self._width = 1
        self._height = 1
        self._scroll = 0 
        self._scroll_previous = 0 
        self._clicked_x = -1
        self._ohlc_previous_lenght = 0
        self._scroll_delay_acquisition = time.time()
        self._mn= -65536
        self._mx= 0
        self._time_range = []
        self._refreshment_required = True
        self._surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, self._width, self._height)
        self._cairo_context = cairo.Context(self._surface)
        self._is_visible = False
        self._callback_local_thread_handler = -1
        super(Gtk.DrawingArea, self).__init__()
        self.set_events(
            Gdk.EventMask.SCROLL_MASK | 
            Gdk.EventMask.BUTTON_PRESS_MASK |
            Gdk.EventMask.BUTTON1_MOTION_MASK | 
            Gdk.EventMask.BUTTON_RELEASE_MASK
        )
        self.__event_handlers = []
        self.connect_event()
        self._callbacks_map = list()

    def _on_click(self, widget, event, propagate=True):
        self.sync_acquire()
        self._local_lock.acquire()
        
        self._scroll_previous = event.x
        self._scroll = 0

        self._master = time.time()
        self._local_lock.release()
        self.sync_release()
        
    def _on_mouse_move_over(self, widget, event, propagate=True):
        self.sync_acquire()
        self._local_lock.acquire()
        if not self._write_buffering_params or self._scroll_delay_acquisition + 0.0125> time.time():
            self._local_lock.release()
            self.sync_release()
            return
        
        clicked_x = self._scroll_previous
        window = self._window
        width = self._width
        x_origin = width - MARGIN_RIGHT
        candlestick_width = x_origin / window

        self._scroll = -int((event.x - clicked_x) / candlestick_width)
        self._scroll_previous = event.x
        self._refreshment_required = True
        self._scroll_delay_acquisition = time.time()
        
        self._master = time.time()
        self._local_lock.release()
        self.sync_release()

    def _on_scroll(self, widget, event, propagate=True):
        self.sync_acquire()
        self._local_lock.acquire()
        if not self._write_buffering_params:
            self._local_lock.release()
            self.sync_release()
            return

        a = event.direction == Gdk.ScrollDirection.DOWN and self._window < self.max_candlesticks
        b = event.direction == Gdk.ScrollDirection.UP and self._window > self.min_candlesticks
        
        if a:
            self._window +=1

        elif b:
            self._window -=1
        
        self._refreshment_required = True
        
        self._master = time.time()
        self._local_lock.release()
        self.sync_release()
    
    def _on_release(self, widget, event, propagate=True):
        self.sync_acquire()
        self._local_lock.acquire()
        self._scroll = 0
        self._scroll_previous = 0
        self._scroll_current = 0
        self.sync_release()
        self._local_lock.release()

    def force_refreshment(self):
        self._local_lock.acquire()
        self._refreshment_required = True
        self._request_refreshment()
        self._local_lock.release()

    def _request_refreshment(self):
        self._local_lock.acquire()
        is_updated = self._data_handler.is_updated_by_indicator(self._indicator_key)

        if is_updated or self._refreshment_required:
            self.__has_acquired_data = True
            self._refreshment_required = False
            self._local_lock.release()
            return True

        self._local_lock.release()

        return False

    def _is_invalid_state(self):
        if self._mn < 0 or self._mx == self._mn:
            return True

        return False
   
    def _get_window_extremums(self):
        return self._data_handler.get_dataset(self._indicator_key).get_window_extremums()

    def _scale_setup(self, height):
        self._data_handler.acquire_lock()
        try:
            self._mn, self._mx, latest, oldest = self._get_window_extremums()
            self._set_time_range(int(oldest), int(latest))
        except ValueError as e:
            self._mn, self._mx, latest, oldest =(-65536, 0, 0, 0)
        
        self._data_handler.release_lock()

        if self._is_invalid_state():
            return

        self._rescale_ruler(height)

    def _set_time_range(self, oldest, latest):
        self._time_range = []
        for t in range(oldest,latest+self._timeframe,self._timeframe):
            self._time_range.append(t)

    def _draw_into_buffer(self):
        self._local_lock.acquire()
        self._surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, self._width, self._height)
        height = self._height
        width = self._width
        window = self._window
        self._local_lock.release()

        self._scale_setup(height)       

        if self._is_invalid_state():
            return 1

        self._cairo_context = cairo.Context(self._surface)
        self._cairo_context.set_source_rgb(1.0,1.0,1.0)
        self._cairo_context.rectangle(0,0, width, height)
        self._cairo_context.fill()

        self._draw_ruler(self._cairo_context, window)
        self._data_handler.acquire_lock()
        for callback_map in self._callbacks_map: # draw indicators there
            try:
                callback_map[1](self, self._cairo_context)

            except KeyError as e:
                pass #print(e)
            
        self._data_handler.release_lock()

        return 0
    
    def get_time_range_index(self, timestamp):
        return self._time_range[::-1].index( (timestamp // self._timeframe) * self._timeframe)

    def _transfert(self):
        self._local_lock.acquire()
        try:
            self._buffer = cairo.ImageSurface.create_for_data(self._surface.get_data(), cairo.FORMAT_ARGB32, self._width, self._height)
        
        except TypeError as e: # buffer might have changed
            pass
            
        self._local_lock.release()
        return False

    def _draw(self, da, ctx):
        self._local_lock.acquire()
        if self._buffer != None:
            if self._buffer.get_width() == self._width and self._buffer.get_height() == self._height:
                self._pb = Gdk.pixbuf_get_from_surface(self._buffer, 0, 0, self._width, self._height)
                Gdk.cairo_set_source_pixbuf(ctx, self._pb, 0, 0)
                ctx.paint()
        self._local_lock.release()

    def _refresh_dimensions(self, widget, event):
        self._local_lock.acquire()
        self._refreshment_required = True
        self._width = self.get_allocation().width
        self._height = self.get_allocation().height
        self._local_lock.release()

    def _rescale_ruler(self, height):
        draw_ruler_x, margin_bottom = self.get_rulers_params()
        self._rescaled_height = height - MARGIN_TOP - margin_bottom
        
        # Compute step and scales
        self._step = 100000
        length = len(np.arange(self._mn, self._mx, self._step))
        
        for factor in [10,5,2.5,2]:
            while length * 15 < self._rescaled_height:
                self._step /= factor
                length = len(np.arange(self._mn, self._mx, self._step))
                
             
            if length * 15 >= self._rescaled_height:
                self._step *= factor
                length = len(np.arange(self._mn, self._mx, self._step))
        
        self._rescaled_mn = 0
        while not (self._rescaled_mn <= self._mn and self._rescaled_mn + self._step > self._mn):
            self._rescaled_mn += self._step
        
        self._rescaled_mx = 0
        while not (self._rescaled_mx > self._mx and self._rescaled_mx - self._step <= self._mx):
            self._rescaled_mx += self._step 
        
        self._divisor = (self._rescaled_mx - self._rescaled_mn)

    def format_y_annotation(self, value):
        return str(value)

    def _do_draw_ruler_y(self, ctx, width, height):
        draw_ruler_x, margin_bottom = self.get_rulers_params()

        ruler = list(np.arange(self._rescaled_mn, self._rescaled_mx, self._step))
        if self._rescaled_mx not in ruler:
            ruler = ruler + [self._rescaled_mx]

        ctx.set_line_width(1)
        for annotation in ruler:
            ctx.set_source_rgb(0.0, 0.0, 0.0)
            position = MARGIN_TOP + self._rescaled_height - ((annotation-self._rescaled_mn) / self._divisor) * self._rescaled_height
            ctx.move_to(width - MARGIN_RIGHT+10, position+5)
            ctx.show_text(self.format_y_annotation(annotation))
            
            ctx.move_to(width - MARGIN_RIGHT, position)
            ctx.line_to(width - MARGIN_RIGHT + 5, position) 
            ctx.stroke()
            ctx.set_source_rgb(0.75, 0.75, 0.75)
            ctx.move_to(0, position)
            ctx.line_to(width - MARGIN_RIGHT, position) 
            ctx.stroke()
        
        ctx.set_source_rgb(0, 0, 0)
        ctx.move_to(width - MARGIN_RIGHT, MARGIN_TOP-1)
        ctx.line_to(width - MARGIN_RIGHT, height - margin_bottom) 
        ctx.line_to(width - MARGIN_RIGHT, height - margin_bottom) 
        ctx.line_to(0, height - margin_bottom) 
        ctx.stroke()

    def _do_draw_ruler_x(self, ctx, width, height, window):
        x_origin = width - MARGIN_RIGHT
        candlestick_width = x_origin / (window - 1)

        draw_ruler_x, margin_bottom = self.get_rulers_params()
        
        # Setting up spacing
        spacing = 1
        while spacing * candlestick_width < 75:
            spacing+=1
               
        i = 0
        # buffering ruler x
        ruler_x_param_v1 = []
        for t in self._time_range[::-1]:
            if t % (spacing * self._timeframe) == 0:
                ruler_x_param_v1.append([i, t, True, None])

            
            else:
                ruler_x_param_v1.append([i, t, False, None])

            t = ruler_x_param_v1[-1][1]
            d, formater = date_formater(t-self._timeframe, t)
            switch_formater = self._switch_formater(formater)
            if switch_formater:
                ruler_x_param_v1[-1][2] = True
                ruler_x_param_v1[-1][3] = True

            i+=1
        ruler_x_param_v2 = [param for param in ruler_x_param_v1 if param[2]]
        ruler_x_param_v3 = []
        for i in range (0, len(ruler_x_param_v2)):
            try:
                if (not ruler_x_param_v2[i-1][3]) and (not ruler_x_param_v2[i+1][3]):
                    ruler_x_param_v3.append(ruler_x_param_v2[i])

            except:
                ruler_x_param_v3.append(ruler_x_param_v2[i])

        for param in ruler_x_param_v3:
            if param[2]:
                i = param[0]
                t = param[1]
                if draw_ruler_x:
                    ctx.set_source_rgb(0, 0, 0)
                    ctx.move_to(x_origin - (i * candlestick_width + candlestick_width / 2) - 1, height - MARGIN_BOTTOM)
                    ctx.line_to(x_origin - (i * candlestick_width + candlestick_width / 2) - 1, height - MARGIN_BOTTOM + 5) 
                    ctx.stroke()

                    d, formater = date_formater(t-self._timeframe, t)
                    if self._switch_formater(formater):
                        ctx.select_font_face("Arial", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)

                    else:
                        ctx.select_font_face("Arial", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)

                    d_width = ctx.text_extents(d)[2]
                    ctx.move_to(x_origin - (i * candlestick_width + candlestick_width / 2) - 1 - d_width/2, height - MARGIN_BOTTOM + 20)
                    ctx.show_text(d)
                
                ctx.set_source_rgb(0.75, 0.75, 0.75)
                ctx.move_to(x_origin - (i * candlestick_width + candlestick_width / 2) - 1, MARGIN_TOP)
                ctx.line_to(x_origin - (i * candlestick_width + candlestick_width / 2) - 1, height - margin_bottom - 1) 
                ctx.stroke()
                
            i+=1

    def _switch_formater(self, formater):
        return formater in ["%Y", "%d %b"] and self._timeframe < 86400

    def _draw_ruler(self, ctx, window):
        self._local_lock.acquire()
        width = self._width
        height = self._height
        try:
            self._local_lock.release()
        
        except RuntimeError:
            pass
        
        ctx.select_font_face("sans", cairo.FONT_SLANT_NORMAL, 
        cairo.FONT_WEIGHT_NORMAL)
        ctx.set_font_size(10)
        ctx.set_antialias(cairo.ANTIALIAS_NONE)

        self._do_draw_ruler_y(ctx, width, height)
        self._do_draw_ruler_x(ctx, width, height, window)

    def get_time_range(self):
        return self._time_range

    def connect_event(self):
        self.__event_handlers = []
        self.__event_handlers.append(self.connect("size-allocate", self._refresh_dimensions))
        self.__event_handlers.append(self.connect("draw", self._draw))
        self.__event_handlers.append(self.connect("scroll-event", self._on_scroll))
        self.__event_handlers.append(self.connect("button-press-event", self._on_click))
        self.__event_handlers.append(self.connect("button-release-event", self._on_release))
        self.__event_handlers.append(self.connect("motion_notify_event", self._on_mouse_move_over))

    def get_master(self):
        self._local_lock.acquire()
        master = self._master
        self._local_lock.release()
        return master

    def get_buffering_params(self):
        self._local_lock.acquire()
        window_length = self._window
        scroll = self._scroll
        self._local_lock.release()
        return (scroll, window_length)
    
    def get_timeframe(self):
        return self._timeframe

    def prepare_dataset(self, scroll, window_length):
        try:
            self._data_handler.prepare_dataset(scroll, window_length)

        except Exception as e:
            if type(e) in [ServerError, APINotAvailable, ResponseIsNotJSON]:
                return e
            
            raise e

    def finalize_buffering(self):
        request_refreshment = self._request_refreshment()
        draw_into_buffer = 0 == self._draw_into_buffer()

        if request_refreshment and draw_into_buffer:
            self._transfert()
            GLib.idle_add(self.queue_draw)

    def get_rulers_params(self):
        self._draw_ruler_x_lock.acquire()
        draw_ruler_x = self._draw_ruler_x
        self._draw_ruler_x_lock.release()
        
        if draw_ruler_x:
            margin_bottom = MARGIN_BOTTOM
        
        else:
            margin_bottom = MARGIN_TOP
        
        return (draw_ruler_x, margin_bottom)
    
    def set_draw_ruler_x(self, value):
        self._draw_ruler_x_lock.acquire()
        self._draw_ruler_x = value
        self._draw_ruler_x_lock.release()

    def get_dataset(self, key):
        return self._data_handler.get_dataset(key)

    def compute_y_coordinate(self, value):
        if value == -1:
            return MARGIN_TOP + self._rescaled_height - 1

        elif value == 0:
            return MARGIN_TOP

        else:
            return MARGIN_TOP + self._rescaled_height - ((value - self._rescaled_mn) / self._divisor) * self._rescaled_height

    def get_window(self):
        self._local_lock.acquire()
        window = self._window
        self._local_lock.release()
        return window

    def get_x_origin(self):
        self._local_lock.acquire()
        x_origin = self._width - MARGIN_RIGHT
        self._local_lock.release()
        return x_origin
        
    def start_widget(self):
        """Start widget refreshment."""
        self._run = True
        self._callback_local_thread_handler = GLib.timeout_add(60, self._local_thread_handler)

    def stop_widget(self):
        """Stop nicely widget."""
        self._run = False
        if self._callback_local_thread_handler != -1:
            GLib.source_remove(self._callback_local_thread_handler)
            self._callback_local_thread_handler = -1

        try:
            self._thread.join()
            
        except RuntimeError:
            pass        

    def set_title(self, title):
        """Set title of the actual chart.
        
        title : str -- A string displayed at the top left of the chart."""
        self._title = title
    
    def set_subtitle(self, subtitle):
        """Set subtitle of the actual chart.
        
        subtitle : str -- A string displayed at the top left of the chart."""
        self._subtitle = subtitle

    def auto_refresh_timeout(self, timeout=1000):
        """Set timeout for chart redrawing.
        
        timeout : int -- expressed in milliseconds. Default is 1000."""
        self._timeout = timeout
        self._timeout = 60 if self._timeout < 60 else self._timeout

    def set_sync_lock(self, lock):
        self._sync_lock = lock

    def set_write_buffering_params(self, value):
        self._local_lock.acquire()
        self._write_buffering_params = value
        self._local_lock.release()

    def sync_acquire(self):
        if self._sync_lock != None:
            self._sync_lock.acquire()
    
    def sync_release(self):
        if self._sync_lock != None:
            self._sync_lock.release()

#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import datetime # for debug

from dsociepy.finance.framework.data.ohlc import OHLC

from dsociepy.finance.charts.chart_canvas import ChartCanvas
from dsociepy.finance.charts.ohlc import draw_ohlc
from dsociepy.finance.charts.ichimoku import draw_ichimoku
from dsociepy.finance.charts.trades import draw_trades
from dsociepy.finance.charts.ranges import draw_ranges

class Price(ChartCanvas):
    def __init__(self, data_handler):
        super(Price, self).__init__(data_handler)
        self._indicator_key = "OHLC"
        self._callbacks_map.append(["Ichimoku", draw_ichimoku, False])
        self._callbacks_map.append(["OHLC", draw_ohlc, False])
        self._callbacks_map.append(["Ranges", draw_ranges, True])
        self._callbacks_map.append(["Trades", draw_trades, True])

    def bind_ichimoku(self):
        self._data_handler.bind_ichimoku()
        self._refreshment_required = True

    def unbind_ichimoku(self):
        self._data_handler.unbind_ichimoku()
        self._refreshment_required = True
    
    def _get_window_extremums(self):
        try:
            a, b, c ,d = self._data_handler.get_dataset(self._callbacks_map[-3][0]).get_window_extremums()

        except Exception as e:
            raise ValueError(e)

        for i in range(0, len(self._callbacks_map)-1):
            if self._callbacks_map[i][0] in ["Trades", "Ranges"]:
                continue

            try:
                A, B, C, D = self._data_handler.get_dataset(self._callbacks_map[i][0]).get_window_extremums()
                if A < a and A != -1:
                    a = A

                if  B > b and B != -1:
                    b = B

                if C > c:
                    c = C
                    d = D

            except KeyError:
                pass
        
        return (a,b,c,d)


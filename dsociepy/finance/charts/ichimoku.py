#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import cairo

TENKAN_SEN = 1
KIJUN_SEN = 2
CHIKOU_SPAN = 3
SSA = 4
SSB = 5

def draw_cloud(cairo_ctx, chart, clusters):

    """ DRAWING """
    
    x_origin = chart.get_x_origin()
    window = chart.get_window()
    candlestick_width = x_origin / (window - 1)

    for cluster in clusters:
        points = cluster[0] + cluster[1][::-1]
        move_to = True
        for point in points:
            try:
                i = chart.get_time_range_index(point[0])

            except Exception as e:
                continue

            y = chart.compute_y_coordinate(point[1])
            x = x_origin - i * candlestick_width - (candlestick_width /2)
            
            if move_to:
                cairo_ctx.move_to(x, y)
                move_to = False

            else:
                cairo_ctx.line_to(x, y)

        cairo_ctx.fill()

        

def draw_line(indicator_index, cairo_ctx, chart, dataset):
    x_origin = chart.get_x_origin()
    window = chart.get_window()
    candlestick_width = x_origin / (window - 1)
    
    draw = False
    previous_y = None
    for index in range(0, len(dataset)):
        try:
            i = chart.get_time_range_index(dataset[index][0])

        except Exception as e:
            continue

        if len(dataset[index]) > 1 :
            if dataset[index][indicator_index] != -1:
                if not draw:
                    y = chart.compute_y_coordinate(dataset[index][indicator_index])
                    x = x_origin - i * candlestick_width - (candlestick_width /2)
                
                    if previous_y != None:
                        previous_x = x_origin - (i-1) * candlestick_width - (candlestick_width/2)
                        cairo_ctx.move_to(previous_x, previous_y)
                        cairo_ctx.line_to(x, y)
                        cairo_ctx.stroke()

                    cairo_ctx.move_to(x, y)
                    draw = True

                else:
                    y = chart.compute_y_coordinate(dataset[index][indicator_index])
                    x = x_origin - i * candlestick_width - (candlestick_width /2)
                    cairo_ctx.line_to(x, y)
                    cairo_ctx.stroke()
                    draw = False
                    previous_y = y
            else:
                draw = False
                previous_y = None
                

        if x_origin - i * candlestick_width < - candlestick_width:
            break

def draw_ichimoku(chart, cairo_ctx):
    """Callback for drawing Ichimoku cloud.
        
    chart : dsociepy.finance.charts.chart_canvas --
    cairo_ctx : cairo.context -- ."""
    dataset_generator = chart.get_dataset("Ichimoku").yield_data()
    
    cairo_ctx.set_source_rgb(0.33, 0.33, 1)
    cairo_ctx.set_line_width(1.5)
    cairo_ctx.set_antialias(cairo.ANTIALIAS_FAST)
    
    dataset = [d for d in dataset_generator]
    
    draw_line(TENKAN_SEN, cairo_ctx, chart, dataset)
    cairo_ctx.set_source_rgb(0.75, 0.25, 0.25)
    draw_line(KIJUN_SEN, cairo_ctx, chart, dataset)
    cairo_ctx.set_source_rgb(0.0, 0.75, 0.50)
    draw_line(CHIKOU_SPAN, cairo_ctx, chart, dataset)
    

    """ CLUSTERING """
    clusters_rise = []
    cluster_rise_SSA = []
    cluster_rise_SSB = []
    
    clusters_fall = []
    cluster_fall_SSA = []
    cluster_fall_SSB = []

    for frame in dataset:
        # reset cluster
        if frame[SSA] == -1 or frame[SSB] == -1:
            if cluster_fall_SSA != []:
                clusters_fall.append([cluster_fall_SSA, cluster_fall_SSB])
                cluster_fall_SSA = []
                cluster_fall_SSB = []
            
            if cluster_rise_SSA != []:
                clusters_rise.append([cluster_rise_SSA, cluster_rise_SSB])
                cluster_rise_SSA = []
                cluster_rise_SSB = []

        if frame[SSA] > frame[SSB]:
            if cluster_fall_SSA != []:
                cluster_fall_SSA.append([frame[0], frame[SSA]])
                cluster_fall_SSB.append([frame[0], frame[SSB]])
                clusters_fall.append([cluster_fall_SSA, cluster_fall_SSB])
                cluster_fall_SSA = []
                cluster_fall_SSB = []
            
            cluster_rise_SSA.append([frame[0], frame[SSA]])
            cluster_rise_SSB.append([frame[0], frame[SSB]])

        else:
            if cluster_rise_SSA != []:
                cluster_rise_SSA.append([frame[0], frame[SSA]])
                cluster_rise_SSB.append([frame[0], frame[SSB]])
                clusters_rise.append([cluster_rise_SSA, cluster_rise_SSB])
                cluster_rise_SSA = []
                cluster_rise_SSB = []
            
            cluster_fall_SSA.append([frame[0], frame[SSA]])
            cluster_fall_SSB.append([frame[0], frame[SSB]])

    if cluster_rise_SSA != []:
        clusters_rise.append([cluster_rise_SSA, cluster_rise_SSB])
    
    if cluster_fall_SSA != []:
        clusters_fall.append([cluster_fall_SSA, cluster_fall_SSB])

    cairo_ctx.set_source_rgba(0.5, 0.75, 0.5, 0.5)
    draw_cloud(cairo_ctx, chart, clusters_rise)

    cairo_ctx.set_source_rgba(0.75, 0.5, 0.5, 0.5)
    draw_cloud(cairo_ctx, chart, clusters_fall)

    cairo_ctx.set_source_rgb(0.25, 0.5, 0.25)
    draw_line(SSA, cairo_ctx, chart, dataset)
    cairo_ctx.set_source_rgb(0.5, 0.25, 0.25)
    draw_line(SSB, cairo_ctx, chart, dataset)
        
        
        

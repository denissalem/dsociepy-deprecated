#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import cairo

from dsociepy.finance.charts.chart_canvas import ChartCanvas

class Volume(ChartCanvas):
    def __init__(self, data_handler):
        super(Volume, self).__init__(data_handler)
        self._indicator_key = "Volume"
        self._callbacks_map.append(["Volume", self.draw_volume, True])

    def format_y_annotation(self, value):
        scales = [ (100000000,'G'), (1000000,'M'), (1000,'K') ]
        for scale in scales:
            new_value =  value / scale[0]
            if new_value >= 1:
                return str(new_value)+scale[1]

        return str(value)

    def draw_volume(self, chart, cairo_ctx):
        """Callback for drawing volume.
        
        cairo_ctx : cairo.context -- ."""
        cairo_ctx.set_antialias(cairo.ANTIALIAS_NONE)

        x_origin = chart.get_x_origin()
        window = chart.get_window()
        bar_width = x_origin / (window-1)
        
        data_set = chart.get_dataset("Volume").yield_data()

        for volume in data_set:
            try:
                i = 1 + chart.get_time_range_index(volume[0])

            except Exception as e:
                continue
            
            if len(volume) > 1 :
                if volume[2]:
                    cairo_ctx.set_source_rgb(0, 0.75, 0)
            
                else:
                    cairo_ctx.set_source_rgb(0.75, 0, 0)

                if volume[1] == 0:
                    volume[1] = -1

                bottom = chart.compute_y_coordinate(-1)
                top = chart.compute_y_coordinate(volume[1])

                cairo_ctx.rectangle(
                    x_origin - i * bar_width,
                    top,
                    bar_width-2,
                    bottom - top
                )
                cairo_ctx.fill()
            
                x = x_origin - i * bar_width + (bar_width-2)/2
            
            else:
                cairo_ctx.set_source_rgb(0, 0, 0)
                cairo_ctx.rectangle(
                    x_origin - i * bar_width,
                    chart.compute_y_coordinate(0),
                    bar_width,
                    chart.compute_y_coordinate(-1) - chart.compute_y_coordinate(0)
                )
                cairo_ctx.fill()
            
            if x_origin - i * bar_width < - bar_width:
                break

#! /usr/bin/python3

#    Copyright 2018 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import cairo
import math

def draw_trades(chart, cairo_ctx):
    cairo_ctx.set_antialias(cairo.ANTIALIAS_FAST)
    """Callback for drawing trades.
        
    cairo_ctx : cairo.context -- ."""

    x_origin = chart.get_x_origin()
    window = chart.get_window()
    candlestick_width = x_origin / (window - 1)
    
    time_range = chart.get_time_range()
    data_set = chart.get_dataset("Trades").yield_trades(time_range[0], time_range[-1])
    
    for marker in data_set:
        try:
            i = 1 + chart.get_time_range_index(marker[0])

        except Exception as e:
            continue

        y = chart.compute_y_coordinate(marker[2])
        x = x_origin - i * candlestick_width + (candlestick_width-2)/2
        cairo_ctx.set_line_width(2)
        cairo_ctx.set_source_rgb(0, 0, 0)
        cairo_ctx.arc(x, y, (candlestick_width-2)/2, 0, 2*math.pi)
        cairo_ctx.stroke_preserve()
        if marker[1] == 'b':
            cairo_ctx.set_source_rgb(0, 0.75, 0)
            
        else:
            cairo_ctx.set_source_rgb(0.75, 0, 0)

        cairo_ctx.fill()
    cairo_ctx.set_antialias(cairo.ANTIALIAS_NONE)

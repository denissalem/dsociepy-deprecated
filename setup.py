#!/usr/bin/python3

#    Copyright 2016, 2017 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup
import os

setup(name="DSOCIEPY.Finance",
    version="0.0.0",
    description="FOSS compliant finance tools from DSOCIETY",
    author="Denis Salem, Dustin X",
    author_email="dsociepy@tuxfamily.org",
    url='https://framagit.org/DenisSalem/dsociepy',
    packages=[
        'dsociepy',
        'dsociepy.finance',
        'dsociepy.finance.charts',
        'dsociepy.finance.framework',
        'dsociepy.finance.framework.data',
        'dsociepy.finance.framework.market_interfaces',
        'dsociepy.finance.framework.server',
        'dsociepy.finance.framework.user_interfaces',
    ],
    license="GNU/GPLv3",
    platforms="Linux",
    long_description="FOSS compliant python tools from DSOCIETY",
    classifiers=[
        "Environment :: Console",
        "Development Status :: 5 - Production/Stable"
    ],
    install_requires=[
        'numpy',
        'pycairo',
        'pycurl',
        'pygobject'
    ],
    scripts=[
        "dsociepy.finance.framework",
        "dsociepy.finance.market_data_server",
        "dsociepy.finance.market_data_server_integrity"
    ],
    data_files = [ (os.path.expanduser("~")+'/.local/share/dsociepy/'+root, [os.path.join(root, f) for f in files])
        for root, dirs, files in os.walk("ressources/finance")
    ]
)
